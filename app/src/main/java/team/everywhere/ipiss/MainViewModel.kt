package team.everywhere.ipiss

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {

    private val _temp = MutableLiveData<Float>()
    val temp get() = _temp
    fun sendTemp(temp: Float){
        _temp.value = temp
    }

    private val _hum = MutableLiveData<Float>()
    val hum get() = _hum
    fun sendHum(hum: Float){
        _hum.value = hum
    }

    private var _scanDevices = MutableLiveData<ArrayList<BluetoothDevice>>()
    val scanDevices get() = _scanDevices
    fun sendDevices(scanDevices: ArrayList<BluetoothDevice>){
        _scanDevices.value = scanDevices
    }

    private var _timeString = MutableLiveData<String>()
    val timeString get() = _timeString
    fun sendTimeString(timeString: String){
        _timeString.value = timeString
    }

    private var _newDevice = MutableLiveData<BluetoothDevice>()
    val newDevice = _newDevice
    fun sendNewDeviceName(newDevice: BluetoothDevice){
//        _newDevice.value = newDevice
        _newDevice.postValue(newDevice)
    }

    private var _updateCalendar = MutableLiveData<String>()
    val updateCalendar = _updateCalendar
    fun sendUpdateCalendar(updateCalendar: String){
        _updateCalendar.value = updateCalendar
    }

    private var _isBluetooth = MutableLiveData<Boolean>()
    val isBluetooth = _isBluetooth
    fun sendIsBluetooth(isBluetooth: Boolean){
        _isBluetooth.value = isBluetooth
    }

    private var _sensitive = MutableLiveData<Float>()
    val sensitive = _sensitive
    fun sendSensitive(sensitive: Float){
        _sensitive.value = sensitive
    }

    private var _isStop = MutableLiveData<Boolean>()
    val isStop = _isStop
    fun sendIsStop(isStop: Boolean){
        _isStop.value = isStop
    }

    //기기 연결중인지
    private var _isConnecting = MutableLiveData<Boolean>()
    val isConnecting = _isConnecting
    fun sendIsConnecting(isConnecting: Boolean){
        _isConnecting.postValue(isConnecting)
    }

    //시작시간 <= 현재시간 <= 종료시간 true false값
    private var _isDisturb = MutableLiveData<Boolean>()
    val isDisturb = _isDisturb
    fun sendIsDisturb(isDisturb: Boolean){
        _isDisturb.value = isDisturb
    }

    //현재시간
    private var _currentTime = MutableLiveData<Int>()
    val realCurrentTime = _currentTime
    fun sendCurrentTime(realCurrentTime: Int){
        _currentTime.postValue(realCurrentTime)
//        _currentTime.value = realCurrentTime
    }

    //방해금지모드 활성화 여부
    private var _isDoNotDisturb = MutableLiveData<Boolean>()
    val isDoNotDisturb = _isDoNotDisturb
    fun sendIsDoNotDisturb(isDoNotDisturb: Boolean){
        _isDoNotDisturb.value = isDoNotDisturb
    }

    //방해금지모드 시작시간
    private var _isDoNotDisturbStart = MutableLiveData<Boolean>()
    val isDoNotDisturbStart = _isDoNotDisturbStart
    fun sendIsDoNotDisturbStart(isDoNotDisturbStart: Boolean){
        _isDoNotDisturbStart.value = isDoNotDisturbStart
    }

    //방해금지모드 종료시간
    private var _isDoNotDisturbEnd = MutableLiveData<Boolean>()
    val isDoNotDisturbEnd = _isDoNotDisturbEnd
    fun sendIsDoNotDisturbEnd(isDoNotDisturbEnd: Boolean){
        _isDoNotDisturbEnd.value = isDoNotDisturbEnd
    }

    //방해금지모드 시작 시간:분
    val disturbStartHour = MutableLiveData<String>()
    val disturbStartMinute = MutableLiveData<String>()
    fun disturbStartText(hour: String, minute: String){
        disturbStartHour.value = hour
        disturbStartMinute.value = minute
    }

    //방해금지모드 종료 시간:분
    val disturbEndHour = MutableLiveData<String>()
    val disturbEndMinute = MutableLiveData<String>()
    fun disturbEndText(hour: String, minute: String){
        disturbEndHour.value = hour
        disturbEndMinute.value = minute
    }

    //온도센서 on/off
    private var _tempSensor = MutableLiveData<Boolean>()
    val tempSensor = _tempSensor
    fun sendIsTempSensor(tempSensor: Boolean){
        _tempSensor.value = tempSensor
    }
}