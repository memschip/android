package team.everywhere.ipiss

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.webkit.WebView
import team.everywhere.ipiss.databinding.ActivityWebViewBinding

class WebViewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWebViewBinding

    companion object {
        const val DEFAULT_URL = "http://www.memschip.kr"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
        bindViews()
    }

    private fun bindViews(){
        binding.addressBar.setOnEditorActionListener { textView, i, keyEvent ->
            if(i== EditorInfo.IME_ACTION_DONE){
                binding.webView.loadUrl(textView.text.toString())
            }
            return@setOnEditorActionListener false
        }

        binding.goBackBtn.setOnClickListener {
            binding.webView.goBack()
        }

        binding.goForwardBtn.setOnClickListener {
            binding.webView.goForward()
        }

        binding.goHomeBtn.setOnClickListener {
            binding.webView.loadUrl(DEFAULT_URL)
        }

        binding.refreshLayout.setOnRefreshListener {
            binding.webView.reload()
        }
    }

    private fun initViews(){
        binding.webView.apply {
            webViewClient = WebViewClient()
            webChromeClient = WebChromeClient()
            loadUrl(DEFAULT_URL)
            settings.javaScriptEnabled = true
        }
    }

    inner class WebViewClient: android.webkit.WebViewClient(){
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            binding.progressBar.show()
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            binding.progressBar.hide()
            binding.refreshLayout.isRefreshing = false
            binding.goBackBtn.isEnabled = binding.webView.canGoBack()
            binding.goForwardBtn.isEnabled = binding.webView.canGoForward()
            binding.addressBar.setText(url)
        }
    }

    inner class WebChromeClient: android.webkit.WebChromeClient(){
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)

            binding.progressBar.progress = newProgress
        }
    }

    override fun onBackPressed() {
        if(binding.webView.canGoBack()){
            binding.webView.goBack()
        }else {
            super.onBackPressed()
        }
    }
}