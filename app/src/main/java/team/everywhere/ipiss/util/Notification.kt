package team.everywhere.ipiss.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Context.VIBRATOR_SERVICE
import android.graphics.Color
import android.media.AudioAttributes
import android.media.MediaMetadataRetriever
import android.media.RingtoneManager
import android.net.Uri
import android.os.Vibrator
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.R
import java.util.*

//TODO: 진동모드 로직
//1.진동모드on + 벨소리 -> 벨소리 + 진동 + 알림
//2.진동모드on + 진동 -> 진동 + 알림
//3.진동모드off + 벨소리 -> 벨소리 + 알림
//4.진동모드off + 진동 -> 무음
object Notification {

    const val TAG = "Notification"

    lateinit var audioAttributes: AudioAttributes
    lateinit var soundUri: Uri
    lateinit var channelId: String

    fun channelSetting(context: Context, sound: String) {
        when (sound) {
            "" -> {
                channelId = MainActivity.CHANNEL_ID
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + "")
            }
            "baby" -> {
                channelId = MainActivity.baby
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "babyCrying" -> {
                channelId = MainActivity.babyCrying
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "babyCrying2" -> {
                channelId = MainActivity.babyCrying2
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "babyCrying3" -> {
                channelId = MainActivity.babyCrying3
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "sound" -> {
                channelId = MainActivity.basicSound
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "sound01" -> {
                channelId = MainActivity.sound01
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "sound03" -> {
                channelId = MainActivity.sound03
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "lightSound" -> {
                channelId = MainActivity.lightSound
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "relaxedSound" -> {
                channelId = MainActivity.relaxedSound
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "relaxingLight" -> {
                channelId = MainActivity.relaxingLight
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "powerfulBeat" -> {
                channelId = MainActivity.powerfulBeat
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
            "powerfulElectric" -> {
                channelId = MainActivity.powerfulElectric
                soundUri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.nosound)
            }
        }

        audioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()

        val channel = NotificationChannel(channelId, MainActivity.CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH).apply {
            lightColor = Color.BLUE
            enableLights(true)
            setSound(soundUri, audioAttributes)
        }

        val manager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(channel)
    }

    fun noSoundChannelSetting(context: Context) {
        val channel = NotificationChannel(
            MainActivity.CHANNEL_ID2,
            MainActivity.CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW
        ).apply {
            lightColor = Color.BLUE
            enableLights(true)
        }

        val manager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(channel)
    }

    fun notificationSetting(context: Context, sound: String, pendingIntent: PendingIntent, title: String, description: String, soundCount: Int, isVibration: Boolean) {
        var notif = NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
        when (sound) {
            "" -> {
                notif = NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
            }
            "baby" -> {
                notif = NotificationCompat.Builder(context, MainActivity.baby)
            }
            "babyCrying" -> {
                notif = NotificationCompat.Builder(context, MainActivity.babyCrying)
            }
            "babyCrying2" -> {
                notif = NotificationCompat.Builder(context, MainActivity.babyCrying2)
            }
            "babyCrying3" -> {
                notif = NotificationCompat.Builder(context, MainActivity.babyCrying3)
            }
            "sound" -> {
                notif = NotificationCompat.Builder(context, MainActivity.basicSound)
            }
            "sound01" -> {
                notif = NotificationCompat.Builder(context, MainActivity.sound01)
            }
            "sound03" -> {
                notif = NotificationCompat.Builder(context, MainActivity.sound03)
            }
            "lightSound" -> {
                notif = NotificationCompat.Builder(context, MainActivity.lightSound)
            }
            "relaxedSound" -> {
                notif = NotificationCompat.Builder(context, MainActivity.relaxedSound)
            }
            "relaxingLight" -> {
                notif = NotificationCompat.Builder(context, MainActivity.relaxingLight)
            }
            "powerfulBeat" -> {
                notif = NotificationCompat.Builder(context, MainActivity.powerfulBeat)
            }
            "powerfulElectric" -> {
                notif = NotificationCompat.Builder(context, MainActivity.powerfulElectric)
            }
        }

        if(isVibration){
            var vibrator = context.getSystemService(VIBRATOR_SERVICE) as Vibrator
            var vibrationPattern = longArrayOf(0, 500, 250, 500)

            notif.apply {
                setContentTitle(title)
                setContentText(description)
                setSmallIcon(R.drawable.logo2)
                setVibrate(vibrationPattern)
                priority = NotificationCompat.PRIORITY_HIGH
                setContentIntent(pendingIntent)
                setAutoCancel(true)
                setOnlyAlertOnce(false)
            }

            vibrator.vibrate(vibrationPattern, -1)
        }else {
            notif.apply {
                setContentTitle(title)
                setContentText(description)
                setSmallIcon(R.drawable.logo2)
                priority = NotificationCompat.PRIORITY_HIGH
                setContentIntent(pendingIntent)
                setAutoCancel(true)
                setOnlyAlertOnce(false)
            }
        }


        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(MainActivity.NOTIF_ID, notif.build())

        CoroutineScope(IO).launch {
            when (sound) {
                "" -> {
                    notif = NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
                }
                "baby" -> {
                    ringtoneSetting(context, R.raw.baby, soundCount)
                }
                "babyCrying" -> {
                    ringtoneSetting(context, R.raw.babycrying, soundCount)
                }
                "babyCrying2" -> {
                    ringtoneSetting(context, R.raw.babycrying2, soundCount)
                }
                "babyCrying3" -> {
                    ringtoneSetting(context, R.raw.nosound, soundCount)
                }
                "sound" -> {
                    ringtoneSetting(context, R.raw.sound, soundCount)
                }
                "sound01" -> {
                    ringtoneSetting(context, R.raw.sound01, soundCount)
                }
                "sound03" -> {
                    ringtoneSetting(context, R.raw.sound03, soundCount)
                }
                "lightSound" -> {
                    ringtoneSetting(context, R.raw.lightsound, soundCount)
                }
                "relaxedSound" -> {
                    ringtoneSetting(context, R.raw.relaxedsound, soundCount)
                }
                "relaxingLight" -> {
                    ringtoneSetting(context, R.raw.relaxinglight, soundCount)
                }
                "powerfulBeat" -> {
                    ringtoneSetting(context, R.raw.powerfulbeat, soundCount)
                }
                "powerfulElectric" -> {
                    ringtoneSetting(context, R.raw.powerfulelectric, soundCount)
                }
            }
        }
    }

    private fun ringtoneSetting(context: Context, raw: Int, soundCount: Int){
        val uriRingtone = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + raw)
        val ringtone = RingtoneManager.getRingtone(context, uriRingtone)

        var audioUri =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.packageName + "/" + raw)
        var mmr = MediaMetadataRetriever()
        mmr.setDataSource(context, audioUri)
        var duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        var millSecond = Integer.parseInt(duration).toLong()

        when(soundCount){
            1 -> {ringtone.play()}
            2 -> {
                ringtone.play()
                Timer().schedule(object : TimerTask() {
                    override fun run() {
                        ringtone.play()
                    }
                },millSecond+1500)
            }
            3 -> {
                ringtone.play()
                Timer().schedule(object : TimerTask() {
                    override fun run() {
                        ringtone.play()

                        Timer().schedule(object : TimerTask() {
                            override fun run() {
                                ringtone.play()
                            }
                        },millSecond+1500)
                    }
                },millSecond+1500)
            }
        }
    }

    fun noSoundNotificationSetting(context: Context, pendingIntent: PendingIntent, title: String, description: String) {
        val notif = NotificationCompat.Builder(context, MainActivity.CHANNEL_ID2)
            .setContentTitle(title)
            .setContentText(description)
            .setSmallIcon(R.drawable.logo2)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(MainActivity.NOTIF_ID, notif)
    }
}