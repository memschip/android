package team.everywhere.ipiss.util

import android.view.View
import team.everywhere.ipiss.databinding.ActivityMainBinding

class ChangeFragmentSetting(var binding: ActivityMainBinding) {
    fun homeFragment(){
        binding.activityToolbar.visibility = View.VISIBLE
        binding.flBNV.visibility = View.VISIBLE
        binding.txtCalendar.visibility = View.VISIBLE
        binding.txtCalendarSub.visibility = View.VISIBLE
        binding.toolbar.visibility = View.VISIBLE
        binding.container.visibility = View.GONE
        binding.bottomNav.visibility = View.VISIBLE

    }

    fun noHomeFragment(){
        binding.activityToolbar.visibility = View.GONE
        binding.flBNV.visibility = View.GONE
        binding.txtCalendar.visibility = View.GONE
        binding.txtCalendarSub.visibility = View.GONE
        binding.toolbar.visibility = View.VISIBLE
        binding.container.visibility = View.VISIBLE
        binding.bottomNav.visibility = View.VISIBLE
    }
}