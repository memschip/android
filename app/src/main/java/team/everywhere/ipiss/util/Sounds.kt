package team.everywhere.ipiss.util

import android.content.Context
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.FragmentSoundsBinding

class Sounds(
    var binding: FragmentSoundsBinding,
    var sound: String,
    var context: Context) {

    fun setSounds(){
        when(sound){
            "" -> {
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false
                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbPowerfulElectric.isChecked = false
                binding.cbLightSound.isChecked = false
            }
            "babyCrying" -> {
                binding.cbBabyCrying.isChecked = true
            }
            "babyCrying2" -> {
                binding.cbBabyCrying2.isChecked = true
            }
            "babyCrying3" -> {
                binding.cbBabyCrying3.isChecked = true
            }
            "baby" -> {
                binding.cbBaby.isChecked = true
            }
            "sound" -> {
                binding.cbSound.isChecked = true
            }
            "sound01" -> {
                binding.cbSound01.isChecked = true
            }
            "sound03" -> {
                binding.cbSound03.isChecked = true
            }
            "relaxedSound" -> {
                binding.cbRelaxedSound.isChecked = true
            }
            "relaxingLight" -> {
                binding.cbRelaxingLight.isChecked = true
            }
            "powerfulBeat" -> {
                binding.cbPowerfulBeat.isChecked = true
            }
            "powerfulElectric" -> {
                binding.cbPowerfulElectric.isChecked = true
            }
            "lightSound" -> {
                binding.cbLightSound.isChecked = true
            }
        }
    }

    fun setSoundBind(){
        binding.cbBabyCrying.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false
                binding.cbSound.isChecked = false

                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "babyCrying")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbBabyCrying2.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false
                binding.cbSound.isChecked = false

                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "babyCrying2")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbBabyCrying3.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBaby.isChecked = false
                binding.cbSound.isChecked = false

                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "babyCrying3")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbBaby.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbSound.isChecked = false

                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "baby")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbSound.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "sound")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbSound01.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "sound01")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbSound03.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbRelaxedSound.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "sound03")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbRelaxedSound.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxingLight.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "relaxedSound")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbRelaxingLight.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbPowerfulBeat.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "relaxingLight")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbPowerfulBeat.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false

                binding.cbPowerfulElectric.isChecked = false
                binding.cbRelaxingLight.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "powerfulBeat")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbPowerfulElectric.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false

                binding.cbPowerfulBeat.isChecked = false
                binding.cbRelaxingLight.isChecked = false
                binding.cbLightSound.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "powerfulElectric")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }

        binding.cbLightSound.setOnCheckedChangeListener { buttonView, b ->
            if(b){
                binding.cbBabyCrying.isChecked = false
                binding.cbBabyCrying2.isChecked = false
                binding.cbBabyCrying3.isChecked = false
                binding.cbBaby.isChecked = false

                binding.cbSound.isChecked = false
                binding.cbSound01.isChecked = false
                binding.cbSound03.isChecked = false
                binding.cbRelaxedSound.isChecked = false

                binding.cbPowerfulBeat.isChecked = false
                binding.cbPowerfulElectric.isChecked = false
                binding.cbRelaxingLight.isChecked = false
                MyApplication.prefs.setSoundName("soundName", "lightSound")
                (context as MainActivity).createNotificationChannel()
            }else {
                MyApplication.prefs.setSoundName("soundName", "")
            }
        }
    }
}