package team.everywhere.ipiss.util

import android.graphics.Color
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R
import team.everywhere.ipiss.databinding.FragmentDoNotDisturbBinding

class DoNotDisturbDay {
    fun everyDayCheck(binding: FragmentDoNotDisturbBinding, isEveryDay: Boolean){
        binding.cbEveryDay.isChecked = isEveryDay
        binding.cbEveryDay.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setEveryDay("everyDay", true)

                MyApplication.prefs.setMonday("monday", true)
                MyApplication.prefs.setTuesday("tuesday", true)
                MyApplication.prefs.setWednesday("wednesday", true)
                MyApplication.prefs.setThursday("thursday", true)
                MyApplication.prefs.setFriday("friday", true)
                MyApplication.prefs.setSaturday("saturday", true)
                MyApplication.prefs.setSunday("sunday", true)

                binding.btnMon.setTextColor(Color.parseColor("#629aff"))
                binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnTues.setTextColor(Color.parseColor("#629aff"))
                binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnWed.setTextColor(Color.parseColor("#629aff"))
                binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnThur.setTextColor(Color.parseColor("#629aff"))
                binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnFri.setTextColor(Color.parseColor("#629aff"))
                binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnSat.setTextColor(Color.parseColor("#629aff"))
                binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_blue)
                binding.btnSun.setTextColor(Color.parseColor("#629aff"))
                binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_blue)
            }else{
                MyApplication.prefs.setEveryDay("everyDay", false)

                MyApplication.prefs.setMonday("monday", false)
                MyApplication.prefs.setTuesday("tuesday", false)
                MyApplication.prefs.setWednesday("wednesday", false)
                MyApplication.prefs.setThursday("thursday", false)
                MyApplication.prefs.setFriday("friday", false)
                MyApplication.prefs.setSaturday("saturday", false)
                MyApplication.prefs.setSunday("sunday", false)

                binding.btnMon.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnTues.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnWed.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnThur.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnFri.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnSat.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_gray)
                binding.btnSun.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_gray)
            }
        }
    }

    fun isMondayCheck(binding: FragmentDoNotDisturbBinding, isMonday: Boolean){
        if(isMonday){
            binding.btnMon.setTextColor(Color.parseColor("#629aff"))
            binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnMon.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnMon.setOnClickListener {
            var isMonday: Boolean = MyApplication.prefs.getMonday("monday", false)
            if(isMonday){
                binding.btnMon.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setMonday("monday", false)
            }else {
                binding.btnMon.setTextColor(Color.parseColor("#629aff"))
                binding.btnMon.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setMonday("monday", true)
            }
        }
    }

    fun isTuesdayCheck(binding: FragmentDoNotDisturbBinding, isTuesday: Boolean){
        if(isTuesday){
            binding.btnTues.setTextColor(Color.parseColor("#629aff"))
            binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnTues.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnTues.setOnClickListener {
            var isTuesday: Boolean = MyApplication.prefs.getTuesday("tuesday", false)
            if(isTuesday){
                binding.btnTues.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setTuesday("tuesday", false)
            }else {
                binding.btnTues.setTextColor(Color.parseColor("#629aff"))
                binding.btnTues.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setTuesday("tuesday", true)
            }
        }
    }

    fun isWednesdayCheck(binding: FragmentDoNotDisturbBinding, isWednesday: Boolean){
        if(isWednesday){
            binding.btnWed.setTextColor(Color.parseColor("#629aff"))
            binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnWed.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnWed.setOnClickListener {
            var isWednesday: Boolean = MyApplication.prefs.getWednesday("wednesday", false)
            if(isWednesday){
                binding.btnWed.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setWednesday("wednesday", false)
            }else {
                binding.btnWed.setTextColor(Color.parseColor("#629aff"))
                binding.btnWed.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setWednesday("wednesday", true)
            }
        }
    }

    fun isThursdayCheck(binding: FragmentDoNotDisturbBinding, isThursday: Boolean){
        if(isThursday){
            binding.btnThur.setTextColor(Color.parseColor("#629aff"))
            binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnThur.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnThur.setOnClickListener {
            var isThursday: Boolean = MyApplication.prefs.getThursday("thursday", false)
            if(isThursday){
                binding.btnThur.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setThursday("thursday", false)
            }else {
                binding.btnThur.setTextColor(Color.parseColor("#629aff"))
                binding.btnThur.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setThursday("thursday", true)
            }
        }
    }

    fun isFridayCheck(binding: FragmentDoNotDisturbBinding, isFriday: Boolean){
        if(isFriday){
            binding.btnFri.setTextColor(Color.parseColor("#629aff"))
            binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnFri.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnFri.setOnClickListener {
            var isFriday: Boolean = MyApplication.prefs.getFriday("friday", false)
            if(isFriday){
                binding.btnFri.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setFriday("friday", false)
            }else {
                binding.btnFri.setTextColor(Color.parseColor("#629aff"))
                binding.btnFri.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setFriday("friday", true)
            }
        }
    }

    fun isSaturdayCheck(binding: FragmentDoNotDisturbBinding, isSaturday: Boolean){
        if(isSaturday){
            binding.btnSat.setTextColor(Color.parseColor("#629aff"))
            binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnSat.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnSat.setOnClickListener {
            var isSaturday: Boolean = MyApplication.prefs.getSaturday("saturday", false)
            if(isSaturday){
                binding.btnSat.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setSaturday("saturday", false)
            }else {
                binding.btnSat.setTextColor(Color.parseColor("#629aff"))
                binding.btnSat.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setSaturday("saturday", true)
            }
        }
    }

    fun isSundayCheck(binding: FragmentDoNotDisturbBinding, isSunday: Boolean){
        if(isSunday){
            binding.btnSun.setTextColor(Color.parseColor("#629aff"))
            binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_blue)
        }else {
            binding.btnSun.setTextColor(Color.parseColor("#d0d5dd"))
            binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_gray)
        }
        binding.btnSun.setOnClickListener {
            var isSunday: Boolean = MyApplication.prefs.getSunday("sunday", false)
            if(isSunday){
                binding.btnSun.setTextColor(Color.parseColor("#d0d5dd"))
                binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_gray)
                MyApplication.prefs.setSunday("sunday", false)
            }else {
                binding.btnSun.setTextColor(Color.parseColor("#629aff"))
                binding.btnSun.setBackgroundResource(R.drawable.day_button_bg_blue)
                MyApplication.prefs.setSunday("sunday", true)
            }
        }
    }
}