package team.everywhere.ipiss.util

import team.everywhere.ipiss.dao.History
import java.text.SimpleDateFormat
import java.util.*

class UpdateTime(val history: History){
    var dt = Date()
    var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    var now = sdf.format(dt).toString()
    var date = now.split(" ")[0] //2022-12-07
    var time = now.split(" ")[1] //13:11:46
    var year = date.split("-")[0].toInt() //2022
    var month = date.split("-")[1].toInt() //12
    var day = date.split("-")[2].toInt() //7
    var hour = time.split(":")[0].toInt() //13
    var minute = time.split(":")[1].toInt() //11
    var second = time.split(":")[2].toInt() //45

    var lastTime = history.updateTime
    var rcvdDate = lastTime.split(" ")[0] //2022-12-07
    var rcvdTiem = lastTime.split(" ")[1] //13:11:46
    var rcvdYear = rcvdDate.split("-")[0].toInt() //2022
    var rcvdMonth = rcvdDate.split("-")[1].toInt() //12
    var rcvdDay = rcvdDate.split("-")[2].toInt() //7
    var rcvdHour = rcvdTiem.split(":")[0].toInt() //13
    var rcvdMinute = rcvdTiem.split(":")[1].toInt() //11
    var rcvdSecond = rcvdTiem.split(":")[2].toInt() //45

    fun getTimeDiffKor(): String{
        return if (rcvdYear == year) {
            if (rcvdMonth == month) {
                if (day == rcvdDay) {
                    if (hour == rcvdHour) {
                        if (minute == rcvdMinute) {
                            if (second == rcvdSecond) {
                                "방금"
                            } else {
                                "${second - rcvdSecond}초 전"
                            }
                        } else {
                            "${minute - rcvdMinute}분 전"
                        }
                    } else {
                        "${hour - rcvdHour}시간 전"
                    }
                } else if (day - rcvdDay == 1) {
                   "어제"
                } else {
                    rcvdDate
                }
            } else {
                rcvdDate
            }
        } else {
            rcvdDate
        }
    }

    fun getTimeDiffEng(): String{
        return if (rcvdYear == year) {
            if (rcvdMonth == month) {
                if (day == rcvdDay) {
                    if (hour == rcvdHour) {
                        if (minute == rcvdMinute) {
                            if (second == rcvdSecond) {
                                "just now"
                            } else {
                                "${second - rcvdSecond} seconds ago"
                            }
                        } else {
                            "${minute - rcvdMinute} minutes ago"
                        }
                    } else {
                        "${hour - rcvdHour} hours ago"
                    }
                } else if (day - rcvdDay == 1) {
                    "yesterday"
                } else {
                    rcvdDate
                }
            } else {
                rcvdDate
            }
        } else {
            rcvdDate
        }
    }
}

class UpdateTime2 {
    var dt = Date()
    var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    var now = sdf.format(dt).toString()
    var date = now.split(" ")[0] //2022-12-07
    var time = now.split(" ")[1] //13:11:46
    var year = date.split("-")[0].toInt() //2022
    var month = date.split("-")[1].toInt() //12
    var day = date.split("-")[2].toInt() //7
    var hour = time.split(":")[0].toInt() //13
    var minute = time.split(":")[1].toInt() //11
    var second = time.split(":")[2].toInt() //45

    fun getToday(country: Boolean): String {
        return if(country){
            "${year}-${month}-${day} ${doDayOfWeek(country)}"
        }else {
            "${year}년 ${month}월 ${day}일 ${doDayOfWeek(country)}요일"
        }
    }

    fun getCurrentTime(): String {
        return if(minute >= 10){
            "${hour}:${minute}"
        }else {
            "${hour}:0${minute}"
        }
    }

    fun doDayOfWeek(country: Boolean): String? {
        val cal: Calendar = Calendar.getInstance()
        var strWeek: String? = null
        val nWeek: Int = cal.get(Calendar.DAY_OF_WEEK)

        if(country){
            if (nWeek == 1) {
                strWeek = "Sunday"
            } else if (nWeek == 2) {
                strWeek = "Monday"
            } else if (nWeek == 3) {
                strWeek = "Tuesday"
            } else if (nWeek == 4) {
                strWeek = "Wednesday"
            } else if (nWeek == 5) {
                strWeek = "Thursday"
            } else if (nWeek == 6) {
                strWeek = "Friday"
            } else if (nWeek == 7) {
                strWeek = "Saturday"
            }
            return strWeek
        }else {
            if (nWeek == 1) {
                strWeek = "일"
            } else if (nWeek == 2) {
                strWeek = "월"
            } else if (nWeek == 3) {
                strWeek = "화"
            } else if (nWeek == 4) {
                strWeek = "수"
            } else if (nWeek == 5) {
                strWeek = "목"
            } else if (nWeek == 6) {
                strWeek = "금"
            } else if (nWeek == 7) {
                strWeek = "토"
            }
            return strWeek
        }
    }
}