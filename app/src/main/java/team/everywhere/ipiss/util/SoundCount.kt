package team.everywhere.ipiss.util

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R
import team.everywhere.ipiss.databinding.FragmentSoundsBinding

class SoundCount(
    var binding: FragmentSoundsBinding,
    var soundCount: Int,
    var context: Context) {

    fun setSoundCount(){
        when(soundCount){
            1 -> {
                binding.rb1.isChecked = true
                binding.rb1.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
            }
            2 -> {
                binding.rb2.isChecked = true
                binding.rb2.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
            }
            3 -> {
                binding.rb3.isChecked = true
                binding.rb3.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
            }
        }
    }

    fun setSoundCountBind(){
        binding.radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            if(radioGroup.id == R.id.radioGroup){
                when(i){
                    R.id.rb1 -> {
                        binding.rb1.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
                        MyApplication.prefs.setSoundCount("soundCount", 1)
                    }
                    R.id.rb2 -> {
                        binding.rb2.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
                        MyApplication.prefs.setSoundCount("soundCount", 2)
                    }
                    R.id.rb3 -> {
                        binding.rb3.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.radio_color))
                        MyApplication.prefs.setSoundCount("soundCount", 3)
                    }
                }
            }
        }
    }
}