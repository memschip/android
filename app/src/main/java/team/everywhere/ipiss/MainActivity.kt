package team.everywhere.ipiss

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application.ActivityLifecycleCallbacks
import android.app.PendingIntent
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.AudioManager
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Switch
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import team.everywhere.ipiss.dao.AppDatabase
import team.everywhere.ipiss.databinding.ActivityMainBinding
import team.everywhere.ipiss.dialog.BleDialog
import team.everywhere.ipiss.dialog.CustomDialog
import team.everywhere.ipiss.dialog.ServiceDialog
import team.everywhere.ipiss.dialog.SoundsFragment
import team.everywhere.ipiss.fragment.*
import team.everywhere.ipiss.service.MyService
import team.everywhere.ipiss.util.BluetoothUtils.Companion.findResponseCharacteristic
import team.everywhere.ipiss.util.ChangeFragmentSetting
import team.everywhere.ipiss.util.Notification
import java.time.LocalDateTime
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.thread
import kotlin.time.Duration.Companion.hours

//1. 최초 앱 실행시 baseDevice가 없기때문에 HomeFragment로 이동한다.

//2. 앱 실행 후 기기연결 -> 디바이스 체크 -> sendIsConnecting에 true값 넣어 기기가 연결중입니다 화면 -> baseDevice에 기기가 등록되고, newDevice 옵저버를 통해 Gatt가 연결된다.
//                   -> sendIsBluetooth에 true값 넣어 toolbar아이콘 변경하고 -> sendIsConnecting false값 넣어서 홈화면 이동 -> setIsFirstStart에 false값 넣어서 이제 앱 실행하면 startScan() 실행된다
//      디바이스 연결되면 왜 홈화면으로 이동해야함?? 기기가 연결된 상태에서 앱을 종료하고 다시 시작하면 baseDevice에 기기가 등록되어 바로 연결을 시작한다. 그 사이에 연결중입니다 화면이 나타나고 연결이되면 홈화면으로 와야하기때문에

//3. 기기연결된 상태에서 앱 종료 후 실행 -> baseDevice가 있기때문에 sendIsConnecting true값 넣어서 기기가 연결중입니다 화면 -> sendIsStop true값 넣어서 30초뒤 스캔을 멈추고, sendIsConnecting에 false값 넣어서 홈화면 이동
//      sendIsStop에 true값 왜 넣어야함?? 만약 앱이 실행되면서 기기와 연결이되지 않으면 홈화면을 띄워야 한다.

//4. 앱이 실행된 상태에서 ble기기를 종료하면 연결이 끊겼습니다 Toast
class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {
    lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private val scope = CoroutineScope(Dispatchers.Default)

    companion object {
        const val TAG = "MainActivity"

        const val SERVICE_UUID = "331a36f5-2459-45ea-9d95-6142f0c4b307"
        const val CHARACTERISTIC_UUID = "a73e9a10-628f-4494-a099-12efaf72258f"
        const val CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"

        val CHANNEL_NAME = "channelName"
        val NOTIF_ID = 0

        val CHANNEL_ID = "vibration"
        val CHANNEL_ID2 = "novibration"

        val baby = "1"
        val babyCrying = "2"
        val babyCrying2 = "3"
        val babyCrying3 = "4"
        val basicSound = "5"
        val sound01 = "6"
        val sound03 = "7"
        val lightSound = "8"
        val relaxedSound = "9"
        val relaxingLight = "10"
        val powerfulBeat = "11"
        val powerfulElectric = "12"
    }

    lateinit var db: AppDatabase
    lateinit var changeFragmentSetting: ChangeFragmentSetting

    var isFirstStart = MyApplication.prefs.getIsFirstStart("isFirstStart", true)
    var isDeviceSearchStart = false

    var baseDevice = MyApplication.prefs.getString("device", "없다")
    var scanResults: ArrayList<BluetoothDevice>? = ArrayList()
    var manager: BluetoothManager? = null
    var bleAdapter: BluetoothAdapter? = null
    var bleGatt: BluetoothGatt? = null
    var isBluetoothDialogAlreadyShown = false

    var isHighTemp = true
    var isLowTemp = true
    var isHumid = true

    var homeFragmentGo = true
    var toastStop = true
    var disconnectToast = true

    var sensitive = 80.0f
    var person = "Child"

    var isVibration = MyApplication.prefs.getIsVibrationStart("isVibration", false)
    var isVibrationStart = MyApplication.prefs.getIsVibrationStart("isVibration", false)

    var language = MyApplication.prefs.getLanguage("language", false)
    var isLanguageChange = false

    var isDoNotDisturb: Boolean = MyApplication.prefs.getDoNotDisturb("disturb", false) //스위치 활성화 여부
    var isDisturb = false // 시작시간 <= 현재시간 <= 종료시간
    var localTime = LocalDateTime.now()
    var currentTime = 0
    var isDisturbDayChecked = false //요일체크 여부

    var isTimeDisturb = true
    var startTime = MyApplication.prefs.getStartTimeDoNotDisturb("startTime", 22)
    var startMinute = MyApplication.prefs.getStartMinuteDoNotDisturb("startMinute", 0)
    var endTime = MyApplication.prefs.getEndTimeDoNotDisturb("endTime", 22)
    var endMinute = MyApplication.prefs.getEndMinuteDoNotDisturb("endMinute", 0)

    var sound = MyApplication.prefs.getSoundName("soundName", "")
    var soundCount: Int = MyApplication.prefs.getSoundCount("soundCount", 1)
    lateinit var audioManager: AudioManager
    var mode = 100

    var isBluetooth = false
    lateinit var notificationLazy: NotificationLazy
    lateinit var notificationLazy2: NotificationLazy2

    val BLEScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            addScanResult(result)
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            super.onBatchScanResults(results)
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.d(TAG, "onScanFailed: 블루투스 스캔 실패 $errorCode")
        }
    }

    val handler = Handler()
    var isInBackground = false

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true) //드로어 홈 버튼 활성화
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_outline_menu) //드로어 홈 버튼 아이콘
        supportActionBar?.setTitle("")

        binding.mainNavigationView.setNavigationItemSelectedListener(this)
        
        //데이터베이스 버전 업데이트
        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE History ADD COLUMN temperature INTEGER NOT NULL default 0")
                database.execSQL("ALTER TABLE History ADD COLUMN humidity INTEGER NOT NULL default 0")
            }
        }

        //데이터베이스 생성
        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "historyDB").addMigrations(MIGRATION_1_2).build()

        manager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bleAdapter = manager!!.adapter

        Log.d(TAG, "onCreate: baseDevice $baseDevice")
        if(baseDevice == "없다"){
            mainViewModel.sendIsConnecting(false)
        }else {
            mainViewModel.sendIsConnecting(true)
            mainViewModel.sendIsStop(true)
        }

        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        showBluetoothDialog()
        createNotificationChannel()
        navSwitch()
        bottomNavMove()

        if(!isFirstStart && baseDevice != "없다"){
            startScan()
        }
    }

    override fun onResume() {
        super.onResume()

        isInBackground = false
        showBluetoothDialog()
        createNotificationChannel()
    }

    override fun onPause() {
        super.onPause()

        isInBackground = true
    }
    
    fun startScan(){
        Log.d(TAG, "스캔 시작")
        val settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
            .build()

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "startScan: 블루투스 권한 거절")
                return
            }
        }

        bleAdapter?.bluetoothLeScanner?.startScan(null, settings, BLEScanCallback)
        mainViewModel.isStop.observe(this){
            if(it){
                handler.postDelayed({
                    CoroutineScope(Dispatchers.IO).launch {
                        stopScan()
                    }
//                    stopScan()
                    if(!isDeviceSearchStart){
                        MyApplication.prefs.setString("device", "없다")
                    }
                },40000)
            }else {
                binding.bottomNav.selectedItemId = R.id.bottom_nav_home
            }
        }
    }

    fun addScanResult(result: ScanResult){
        val device = result.device
        val deviceAddress = device.address
        for(dev in scanResults!!){
            if(dev.address == deviceAddress) return
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                return
            }
        }

        //BleDialog RecyclerView에 값 넘겨준다
        if(result.device.name != null){
            Log.d(TAG, "addScanResult: device.name ${device.name}")
            scanResults?.add(result.device)
            scope.launch {
                withContext(Dispatchers.Main){
                    if(scanResults?.size != 0){
                        mainViewModel.sendDevices(scanResults!!)
                    }
                }
            }
        }

        //만약에 앱 종료전에 연결된 기기가 있다면 앱 실행시 자동으로 연결해준다.
        Log.d(TAG, "addScanResult: scanResults.size ${scanResults?.size!!}, ${isDeviceSearchStart}")
        if(isDeviceSearchStart){
            Log.d(TAG, "디바이스 찾기 버튼 클릭해서 실행(자동연결방지)")
        }else {
            if(scanResults!!.size != 0){
                for(i in 0 until scanResults?.size!!){
                    if(scanResults!!.isNotEmpty()){
                        if(scanResults!![i].name == baseDevice){
                            if(scanResults?.size != 0){
                                bleGatt = scanResults!![i].connectGatt(applicationContext, false, gattClientCallback)
                            }
                        }
                    }
                }
            }else {
                Log.d(TAG, "addScanResult: scanResults.size 없다")
            }
        }

        mainViewModel.newDevice.observe(this@MainActivity){
            bleGatt = null
            bleGatt = it.connectGatt(applicationContext, false, gattClientCallback)
        }
    }

    fun stopScan(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                return
            }
        }

        Log.d(TAG, "stopScan: 스캔 정지")
        bleAdapter?.bluetoothLeScanner?.stopScan(BLEScanCallback)
        scanResults?.clear()


        //TODO : 다비이스 연결 후 앱 종료, 재실행시 40초 이내에 연결이 안되면 자동으로 홈 화면 이동
        Log.d(TAG, "stopScan: $homeFragmentGo")
        if(homeFragmentGo){
            mainViewModel.sendIsConnecting(false)
        }
    }

    private val gattClientCallback = object : BluetoothGattCallback() {
        //원격 GATT 서버와 연결상태가 변하면 나타내는 콜백입니다.
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_FAILURE ${BluetoothGatt.GATT_FAILURE}") //257
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_SUCCESS ${BluetoothGatt.GATT_SUCCESS}") //0
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_CONNECTION_CONGESTED ${BluetoothGatt.GATT_CONNECTION_CONGESTED}") //143
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION ${BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION}") //5
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_INSUFFICIENT_AUTHORIZATION ${BluetoothGatt.GATT_INSUFFICIENT_AUTHORIZATION}") //8
//            Log.d(TAG, "버전별상태 BluetoothGatt.GATT_READ_NOT_PERMITTED ${BluetoothGatt.GATT_READ_NOT_PERMITTED}") //2
//            Log.d(TAG, "버전별상태 BluetoothProfile.STATE_CONNECTED ${BluetoothProfile.STATE_CONNECTED}") //2
//            Log.d(TAG, "버전별상태 BluetoothProfile.STATE_DISCONNECTED ${BluetoothProfile.STATE_DISCONNECTED}") //0
//
//            Log.d(TAG, "버전별상태 Build.VERSION.SDK_INT ${Build.VERSION.SDK_INT}")  //30, 30, 33
//            Log.d(TAG, "버전별상태 Build.VERSION_CODES.O ${Build.VERSION_CODES.O}")  //26, 26, 26
//            Log.d(TAG, "버전별상태 Build.VERSION_CODES.S ${Build.VERSION_CODES.S}")  //31, 31, 31

            if(status == 19 && newState == 0){
                Log.d(TAG, "onConnectionStateChange: 실패2!!")
                CoroutineScope(Main).launch {
                    if(toastStop){
                        if ((application as MyApplication).isAppInBackground()) {
                            Log.d(TAG, "onCreate: 백그라운드")
                        } else {
                            val manager = supportFragmentManager
                            CustomDialog(resources.getString(R.string.Disconnected), resources.getString(R.string.ReConnect)).show(manager, "customDialog")
                            binding.bottomNav.selectedItemId = R.id.bottom_nav_home
                        }
                        toastStop = false
                        MyApplication.prefs.setString("device", "없다")
                        withContext(IO){
                            disconnectGattServer()
                        }
                    }
                }

                isBluetooth = false
                MyApplication.prefs.setString("device", "없다")
                CoroutineScope(Main).launch {
                    mainViewModel.sendIsBluetooth(false)
                }
            }
            
            if(status == 133){
                Log.d(TAG, "onConnectionStateChange: 133에러")
//                if(disconnectToast){
//                    if ((application as MyApplication).isAppInBackground()) {
//                        Log.d(TAG, "onCreate: 백그라운드")
//                    } else {
//                        Toast.makeText(this@MainActivity, "기기를 다시 연결해주세요", Toast.LENGTH_SHORT).show()
//                        disconnectToast = false
//                    }
//                }
            }

            bleGatt = gatt
            if(status == BluetoothGatt.GATT_FAILURE){
//                Log.d(TAG, "onConnectionStateChange: 실패")
//                CoroutineScope(Dispatchers.IO).launch {
//                    disconnectGattServer()
//                }
//                CoroutineScope(Dispatchers.Main).launch {
//                    mainViewModel.sendIsConnecting(true)
//                }
                return
            }else if(status != BluetoothGatt.GATT_SUCCESS){
                MyApplication.prefs.setString("device", "없다")
                mainViewModel.sendIsConnecting(false)
                CoroutineScope(Dispatchers.IO).launch {
                    disconnectGattServer()
                }
                return
            }else {
                Log.d(TAG, "onConnectionStateChange: 실패는 아니다")
//                mainViewModel.sendIsConnecting(true)
            }

            if(newState == BluetoothProfile.STATE_CONNECTED){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                    if (ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                        return
                    }
                }

                Log.d(TAG, "onConnectionStateChange: 연결3")
                disconnectToast = true
                if(bleGatt?.device != null){
                    CoroutineScope(Dispatchers.IO).launch {
                        MyApplication.prefs.setString("device", bleGatt?.device!!.name)
                    }
                }

                toastStop = true
                MyApplication.prefs.setIsFirstStart("isFirstStart", false)
                homeFragmentGo = false
                isBluetooth = true
                CoroutineScope(Main).launch {
                    mainViewModel.sendIsBluetooth(true)
                    mainViewModel.sendIsConnecting(false)
                }

                CoroutineScope(Main).launch {
//                    mainViewModel.sendIsConnecting(false)
                    delay(500)
                    withContext(IO){
                        val intent = Intent(this@MainActivity, MyService::class.java)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                            applicationContext.startService(intent)
                        }else {
                            applicationContext.startService(intent)
                        }
                    }
                }
                //discoverService()함수는 원격 장치에서 제공하는 서버를 검색한다. 검색이 완료되면 onServiceDiscovered()함수가 호출된다
                CoroutineScope(Main).launch {
                    gatt.discoverServices()
                }
            }else if(newState == BluetoothProfile.STATE_DISCONNECTED){
                Log.d(TAG, "onConnectionStateChange: 끊김3")

                CoroutineScope(Main).launch {
                    if(toastStop){
                        if ((application as MyApplication).isAppInBackground()) {
                            Log.d(TAG, "onCreate: 백그라운드")
                        } else {
                            val manager = supportFragmentManager
                            CustomDialog(resources.getString(R.string.Disconnected), resources.getString(R.string.ReConnect)).show(manager, "customDialog")
                            binding.bottomNav.selectedItemId = R.id.bottom_nav_home
                        }
                        MyApplication.prefs.setString("device", "없다")
                        mainViewModel.sendIsConnecting(false)
                        toastStop = false
                        withContext(IO){
                            disconnectGattServer()
                        }
                    }
                }

                MyApplication.prefs.setString("device", "없다")
                Log.d(TAG, "onConnectionStateChange: device ${MyApplication.prefs.getString("device", "없다")}")
                isBluetooth = false
                CoroutineScope(Main).launch {
                    mainViewModel.sendIsBluetooth(false)
                    binding.clNull.visibility = View.VISIBLE
                }
            }
        }

        //원격 장치에서 제공하는 서버를 발견했을때 호출된다.
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if(status != BluetoothGatt.GATT_SUCCESS){
                Log.d(TAG, "onConnectionStateChange: 끊김4")
                return
            }
            val respCharacteristic = gatt?.let { findResponseCharacteristic(it) }

            if (respCharacteristic == null) {
                Log.e(TAG, "Unable to find cmd characteristic")
                CoroutineScope(IO).launch { disconnectGattServer() }
                return
            }

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                if (ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
            }

            CoroutineScope(IO).launch {
                gatt.setCharacteristicNotification(respCharacteristic, true)
            }

            // UUID for notification
            val descriptor: BluetoothGattDescriptor = respCharacteristic.getDescriptor(
                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG)
            )
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE

            CoroutineScope(Main).launch {
                gatt.writeDescriptor(descriptor)
            }
        }

        //Ble에서 Chracteristic은 특성이다
        @Deprecated("Deprecated in Java")
        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            Log.d(TAG, "onConnectionStateChange: 연결5")
            readCharacteristic(characteristic)
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            if(status == BluetoothGatt.GATT_SUCCESS){
                Log.d(TAG, "onConnectionStateChange: 연결6")
            }else {
//                CoroutineScope(IO).launch { disconnectGattServer() }
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            if(status == BluetoothGatt.GATT_SUCCESS){
                readCharacteristic(characteristic)
            }
        }
    }

    //Gatt연결 후 데이터 읽어옴
    private fun readCharacteristic(characteristic: BluetoothGattCharacteristic?) {
        val msg = characteristic?.getStringValue(0)
        Log.d(TAG, "read msg: $msg")
        msg?.let {
            if (it.contains(", ")) {
                var stHumid = it.split(", ")[0]
                var stTprt = it.split(", ")[1]
                stHumid = stHumid.substring(2, stHumid.length)
                stTprt = stTprt.substring(2, stTprt.length)
                var humid = stHumid.toFloat()
                var tprt = stTprt.toFloat()

                if (humid >= 100.0f) {
                    humid = 99.9f
                }

                startTime = MyApplication.prefs.getStartTimeDoNotDisturb("startTime", 22)
                startMinute = MyApplication.prefs.getStartMinuteDoNotDisturb("startMinute", 0)
                endTime = MyApplication.prefs.getEndTimeDoNotDisturb("endTime", 22)
                endMinute = MyApplication.prefs.getEndMinuteDoNotDisturb("endMinute", 0)
                //00시는 24시를 말한다. 00시 입력시 startTime값 0으로 나온다
                var disturbStart = if(startTime == 0){
                    24*60+startMinute
                }else {
                    startTime*60+startMinute
                }

//                CoroutineScope(Dispatchers.Default).launch {
//                    Timer().scheduleAtFixedRate(object : TimerTask() {
//                        override fun run() {
//                            localTime = LocalDateTime.now() //2023-09-05T11:37:10.137733
//                            currentTime = localTime.hour*60+localTime.minute
//                            Log.d(TAG, "run: ${localTime.hour}") //8
//                        }
//                    },0, 2000)
//                }

                handler.postDelayed({
                    localTime = LocalDateTime.now()
                    currentTime = localTime.hour*60+localTime.minute
                    Log.d(TAG, "readCharacteristic: currentTime $currentTime")
                },2000)

                var disturbEnd = if(endTime == 0){
                    24*60+endMinute
                }else {
                    endTime*60+endMinute
                }

                isDisturb = currentTime in disturbStart..disturbEnd

                doDayOfWeek()

                var isTempSensor: Boolean = MyApplication.prefs.getIsTempSensorStart("isTempStart", true)
                CoroutineScope(Main).launch {
                    mainViewModel.sendTemp(tprt)
                    mainViewModel.sendHum(humid)

                    mainViewModel.isDoNotDisturb.observe(this@MainActivity){ b ->
                        isDoNotDisturb = b
                        Log.d(TAG, "readCharacteristic: isDoNotDisturb $isDoNotDisturb")
                    }
                    
                    mainViewModel.tempSensor.observe(this@MainActivity){ b ->
                        isTempSensor = b
                        Log.d(TAG, "readCharacteristic: isTempSensor $isTempSensor")
                    }
                }

                Log.d(TAG, "asdqwe 활성화 $isDoNotDisturb, 요일 $isDisturbDayChecked, 시간 $isDisturb")

                //TODO: 영/유아 고령자 체온별 알림
                CoroutineScope(Main).launch {
                    person = MyApplication.prefs.getPersonIsChecked("isPerson", "Child")
                }

                Log.d(TAG, "readCharacteristic: isTempSensor $isTempSensor")
                if(person == "Child"){
                    if(23.0 <= tprt && tprt <= 35.0){
                        isHighTemp = true
                        isLowTemp = true
                        notificationLazy2 = NotificationLazy2()
                    }

                    //비활성이면 무조건 알림보내라
                    if(tprt > 35.0f && isHighTemp && !isDoNotDisturb && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    //활성화상태라도 요일체크 없으면 알림보내라
                    }else if(tprt > 35.0f && isHighTemp && !isDisturbDayChecked && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    //활성화상태이고 요일체크가 있어도 시작시간 <= 현재시간 <= 종료시간이 false 이면 알림보내라
                    }else if(tprt > 35.0f && isHighTemp && !isDisturb && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    }

                    if(tprt < 23.0f && isLowTemp && !isDoNotDisturb && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }else if(tprt < 23.0f && isLowTemp && !isDisturbDayChecked && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }else if(tprt < 23.0f && isLowTemp && !isDisturb && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }
                }else {
                    if(23.0 <= tprt && tprt <= 35.0){
                        isHighTemp = true
                        isLowTemp = true
                        notificationLazy2 = NotificationLazy2()
                    }

                    if(tprt > 35.0f && isHighTemp && !isDoNotDisturb && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    }else if(tprt > 35.0f && isHighTemp && !isDisturbDayChecked && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    }else if(tprt > 35.0f && isHighTemp && !isDisturb && isTempSensor){
                        runBlocking {
//                            highTempNotification
                            notificationLazy2.highTempNotification
                        }
                    }

                    if(tprt < 23.0f && isLowTemp && !isDoNotDisturb && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }else if(tprt < 23.0f && isLowTemp && !isDisturbDayChecked && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }else if(tprt < 23.0f && isLowTemp && !isDisturb && isTempSensor){
                        runBlocking {
//                            lowTempNotification
                            notificationLazy2.lowTempNotification
                        }
                    }
                }

                //TODO: 영/유아, 고령자 습도별 알림
                CoroutineScope(Main).launch {
                    sensitive = MyApplication.prefs.getSensitive("sensitive", 80.0f)
                    if(humid <= sensitive-0.5f){
                        isHumid = true
                        notificationLazy = NotificationLazy()
                    }
                }

                if(humid >= sensitive && isHumid && !isDoNotDisturb){
                    runBlocking {
//                        highHumidNotification
                        notificationLazy.highHumidNotification
                    }
                }else if(humid >= sensitive && isHumid && !isDisturbDayChecked){
                    runBlocking {
//                        highHumidNotification
                        notificationLazy.highHumidNotification
                    }
                }else if(humid >= sensitive && isHumid && !isDisturb){
                    runBlocking {
//                        highHumidNotification
                        notificationLazy.highHumidNotification
                    }
                }
            }
        }
    }

    suspend fun disconnectGattServer(){
        if(bleGatt != null){
            Log.d(TAG, "disconnectGattServer: 연결종료")
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
            }

            CoroutineScope(Main).launch {
                val intent = Intent(this@MainActivity, MyService::class.java)
                stopService(intent)
                withContext(Dispatchers.Default){
                    stopScan()
                }
            }

            bleGatt?.disconnect()
            bleGatt?.close()
            stopScan()
        }
    }

    //방해금지모드 요일값 맞추기
    private fun doDayOfWeek(){
        var isEveryday: Boolean = MyApplication.prefs.getEveryDay("everyDay", false)
        var isSunday: Boolean = MyApplication.prefs.getSunday("sunday", false)
        var isMonday: Boolean = MyApplication.prefs.getMonday("monday", false)
        var isTuesday: Boolean = MyApplication.prefs.getTuesday("tuesday", false)
        var isWednesday: Boolean = MyApplication.prefs.getWednesday("wednesday", false)
        var isThursday: Boolean = MyApplication.prefs.getThursday("thursday", false)
        var isFriday: Boolean = MyApplication.prefs.getFriday("friday", false)
        var isSaturday: Boolean = MyApplication.prefs.getSaturday("saturday", false)

        Log.d(TAG, "doDayOfWeek: isEveryday $isEveryday, $isSunday, $isTuesday, $isWednesday, $isThursday, $isFriday, $isSaturday, $isSunday")
        
        //오늘이 무슨 요일인지 알려준다
        val cal: Calendar = Calendar.getInstance()
        var strWeek: String? = null
        val nWeek: Int = cal.get(Calendar.DAY_OF_WEEK)
        if (nWeek == 1) {
            strWeek = "일"
            isDisturbDayChecked = isSunday
        } else if (nWeek == 2) {
            strWeek = "월"
            isDisturbDayChecked = isMonday
        } else if (nWeek == 3) {
            strWeek = "화"
            isDisturbDayChecked = isTuesday
        } else if (nWeek == 4) {
            strWeek = "수"
            isDisturbDayChecked = isWednesday
        } else if (nWeek == 5) {
            strWeek = "목"
            isDisturbDayChecked = isThursday
        } else if (nWeek == 6) {
            strWeek = "금"
            isDisturbDayChecked = isFriday
        } else if (nWeek == 7) {
            strWeek = "토"
            isDisturbDayChecked = isSaturday
        } else {
            isDisturbDayChecked = isEveryday
        }
    }

    fun createNotificationChannel(){
        //IMPORTANCE_HIGH는 진동도오고 화면에도 뜬다.  IMPORTANCE_DEFAULT는 진동오고 상태바에만 뜬다.  IMPORTANCE_LOW는 진동안오고 상태바에만 뜬다
        mode = audioManager.ringerMode
        isVibration = MyApplication.prefs.getIsVibrationStart("isVibration", false)

        Log.d(TAG, "createNotificationChannel: $mode, $isVibration")
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            if(isVibration){
                sound = MyApplication.prefs.getSoundName("soundName", "")
                Log.d(TAG, "createNotificationChannel: $sound")
                Notification.channelSetting(applicationContext, sound)
            }else {
                if(mode == 2){
                    sound = MyApplication.prefs.getSoundName("soundName", "")
                    Notification.channelSetting(applicationContext, sound)
                }else if(mode == 1){
                    Notification.noSoundChannelSetting(applicationContext)
                }
            }
        }
    }

    //툴바 아이콘 클릭
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(isBluetooth){
            menuInflater.inflate(R.menu.toolbar_menu, menu)
        }else {
            menuInflater.inflate(R.menu.toolbar_menu2, menu)
        }

        if(language){
            val menuItem = menu!!.findItem(R.id.toolbar_menu_language)
            menuItem.setIcon(resources.getDrawable(R.drawable.usa))
        }else {
            val menuItem = menu!!.findItem(R.id.toolbar_menu_language)
            menuItem.setIcon(resources.getDrawable(R.drawable.korea))
        }

        mainViewModel.isBluetooth.observe(this@MainActivity){
            if(it){
                val menuItem = menu.findItem(R.id.toolbar_menu_bluetooth)
                menuItem.setIcon(resources.getDrawable(R.drawable.icon_bluetooth_on))
            }else {
                val menuItem = menu.findItem(R.id.toolbar_menu_bluetooth)
                menuItem.setIcon(resources.getDrawable(R.drawable.icon_bluetooth_off))
            }
        }
        return true
    }

    //Toolbar Item Selected
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                binding.mainDrawerLayout.openDrawer(GravityCompat.START)
            }
            R.id.toolbar_menu_language -> {
                if(isBluetooth){
                    if(language){
                        Toast.makeText(applicationContext, "Please disconnect the device", Toast.LENGTH_SHORT).show()
                    }else {
                        Toast.makeText(this, "디바이스 연결을 종료해주세요.", Toast.LENGTH_SHORT).show()
                    }
                }else {
                    isLanguageChange = true
                    if(language){
                        val configuration = Configuration()
                        configuration.locale = Locale.KOREA
                        resources.updateConfiguration(configuration, resources.displayMetrics)
                        MyApplication.prefs.setLanguage("language", false)
                    }else {
                        val configuration = Configuration()
                        configuration.locale = Locale.US
                        resources.updateConfiguration(configuration, resources.displayMetrics)
                        MyApplication.prefs.setLanguage("language", true)
                    }

                    recreate()

                }
                return super.onOptionsItemSelected(item)
            }
            R.id.toolbar_menu_bluetooth -> {
                return super.onOptionsItemSelected(item)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_home -> {
                binding.bottomNav.selectedItemId = R.id.bottom_nav_home
                if(isBluetooth){
                    binding.clNull.visibility = View.GONE
                }else {
                    binding.clNull.visibility = View.VISIBLE
                }
            }
            R.id.nav_monitoring -> {
                binding.bottomNav.selectedItemId = R.id.bottom_nav_monitoring
                binding.clNull.visibility = View.GONE
            }
            R.id.nav_setting -> {
                binding.bottomNav.selectedItemId = R.id.bottom_nav_setting
                binding.clNull.visibility = View.GONE
            }
            R.id.nav_connection -> {
                isDeviceSearchStart = true
                scanResults?.clear()
                val manager = supportFragmentManager
                BleDialog().show(manager, "bleDialog")
                startScan()
            }
            R.id.nav_sound -> {
                val manager = supportFragmentManager
                SoundsFragment().show(manager, "soundsFragment")
            }
            R.id.nav_service -> {
                val manager = supportFragmentManager
                ServiceDialog().show(manager, "serviceDialog")
            }
        }
        binding.mainDrawerLayout.closeDrawers()
        return false
    }

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    fun navSwitch(){
        val switch = binding.mainNavigationView.menu.findItem(R.id.nav_vibration).actionView?.findViewById<Switch>(R.id.nav_switch)
        isVibrationStart = MyApplication.prefs.getIsVibrationStart("isVibration", false)
        switch?.isChecked = isVibrationStart
        switch?.let{
            switch.setOnCheckedChangeListener { buttonView, isChecked ->
                if(isChecked){
                    MyApplication.prefs.setIsVibrationStart("isVibration", true)
                }else {
                    MyApplication.prefs.setIsVibrationStart("isVibration", false)
                }

                createNotificationChannel()
            }
        }
    }

    private fun bottomNavMove(){
        changeFragmentSetting = ChangeFragmentSetting(binding)
        binding.bottomNav.setOnItemSelectedListener { item ->
            when(item.itemId){
                R.id.bottom_nav_home -> {
                    thread {
                        runOnUiThread {
                            if(isBluetooth){
                                binding.clNull.visibility = View.GONE
                            }else {
                                binding.clNull.visibility = View.VISIBLE
                            }
                            changeFragmentSetting.homeFragment()
                            supportFragmentManager
                                .beginTransaction()
                                .replace(R.id.flBNV, HomeFragment())
                                .commit()
                        }
                    }
                }
                R.id.bottom_nav_monitoring -> {
                    thread {
                        runOnUiThread {
                            binding.clNull.visibility = View.GONE
                            changeFragmentSetting.noHomeFragment()
                            changeFragment(MonitoringFragment())
                        }
                    }
                }
                R.id.bottom_nav_setting -> {
                    thread {
                        runOnUiThread {
                            binding.clNull.visibility = View.GONE
                            changeFragmentSetting.noHomeFragment()
                            changeFragment(SettingFragment())
                        }
                    }
                }
                else -> {
                    thread {
                        runOnUiThread {
                            binding.clNull.visibility = View.GONE
                            changeFragmentSetting.noHomeFragment()
                            changeFragment(DocumentFragment())
                        }
                    }
                }
            }
            true
        }

        mainViewModel.isConnecting.observe(this){
            if(it){
                scope.launch {
                    withContext(Dispatchers.Main){
                        changeFragment(ConnectFragment())
                        changeFragmentSetting.noHomeFragment()
                        binding.clNull.visibility = View.GONE
                        binding.bottomNav.visibility = View.GONE
                    }
                }
            }else {
                scope.launch {
                    withContext(Dispatchers.Main){
                        binding.bottomNav.selectedItemId = R.id.bottom_nav_home
                    }
                }
            }
        }
    }

    private fun changeFragment(fragment: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

    private fun showBluetoothDialog(){
        if(!bleAdapter!!.isEnabled){
            if(!isBluetoothDialogAlreadyShown){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                    //안드로이드 버전 12 이상부터는 블루투스가 꺼져있으면 설정창으로 이동한다.
                    val enableBluetoothIntent = Intent(Settings.ACTION_BLUETOOTH_SETTINGS)
                    startBluetoothForResult.launch(enableBluetoothIntent)
                    isBluetoothDialogAlreadyShown = true
                }else {
                    val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startBluetoothForResult.launch(enableBluetoothIntent)
                    isBluetoothDialogAlreadyShown = true
                }
            }
        }
    }

    private val startBluetoothForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        isBluetoothDialogAlreadyShown = false
        if(result.resultCode != RESULT_OK){
            showBluetoothDialog()
        }
    }

    inner class NotificationLazy {
        val highHumidNotification by lazy {
            soundCount = MyApplication.prefs.getSoundCount("soundCount", 1)
            val intent = Intent(this@MainActivity, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(this@MainActivity, 0 , intent, PendingIntent.FLAG_IMMUTABLE)

            mode = audioManager.ringerMode
            if(isVibration){
                Log.d(TAG, "호출입니다~~~~~~: ")
                sound = MyApplication.prefs.getSoundName("soundName", "")
                Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.app_name), resources.getString(R.string.Change_Diaper), soundCount, true)
            }else {
                if(mode == 2){ //벨소리
                    sound = MyApplication.prefs.getSoundName("soundName", "")
                    Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.app_name), resources.getString(R.string.Change_Diaper), soundCount, false)

                }else if(mode == 1){ // 진동
                    Notification.noSoundNotificationSetting(applicationContext, pendingIntent, resources.getString(R.string.app_name), resources.getString(R.string.Change_Diaper))
                }
            }

            CoroutineScope(Main).launch {
                if(isHumid){
                    if(!isInBackground){
                        val manager = supportFragmentManager
                        CustomDialog(resources.getString(R.string.Change_Diaper), resources.getString(R.string.Please_check_the_diaper)).show(manager, "customDialog")
                    }

                    mainViewModel.sendUpdateCalendar("등록")
                    isHumid = false
                }
            }
        }
    }

    inner class NotificationLazy2 {
        val highTempNotification by lazy {
            soundCount = MyApplication.prefs.getSoundCount("soundCount", 1)
            val intent = Intent(this@MainActivity, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(this@MainActivity, 0 , intent, PendingIntent.FLAG_IMMUTABLE)

            //벨소리 여부
            mode = audioManager.ringerMode
            if(isVibration){
                sound = MyApplication.prefs.getSoundName("soundName", "")
                Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.High_Temper), resources.getString(R.string.High_Temper_Message), soundCount, true)
            }else {
                //2 벨소리, 1 진동
                if(mode == 2){
                    sound = MyApplication.prefs.getSoundName("soundName", "")
                    Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.High_Temper), resources.getString(R.string.High_Temper_Message), soundCount, false)
                }else if(mode == 1){
                    Notification.noSoundNotificationSetting(applicationContext, pendingIntent, resources.getString(R.string.High_Temper), resources.getString(R.string.High_Temper_Message))
                }
            }

            CoroutineScope(Main).launch {
                if(!isInBackground){
                    val manager = supportFragmentManager
                    CustomDialog(resources.getString(R.string.Temperature_is_too_high), resources.getString(R.string.Please_check_the_diaper)).show(manager, "customDialog")
                }
            }

            isHighTemp = false
        }


        val lowTempNotification by lazy {
            soundCount = MyApplication.prefs.getSoundCount("soundCount", 1)
            val intent = Intent(this@MainActivity, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(this@MainActivity, 0 , intent, PendingIntent.FLAG_IMMUTABLE)

            mode = audioManager.ringerMode
            if(isVibration){
                sound = MyApplication.prefs.getSoundName("soundName", "")
                Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.Low_Temper), resources.getString(R.string.Low_Temper_Message), soundCount, true)
            }else {
                if(mode == 2){
                    sound = MyApplication.prefs.getSoundName("soundName", "")
                    Notification.notificationSetting(applicationContext, sound, pendingIntent, resources.getString(R.string.Low_Temper), resources.getString(R.string.Low_Temper_Message), soundCount, false)
                }else if(mode == 1){
                    Notification.noSoundNotificationSetting(applicationContext, pendingIntent, resources.getString(R.string.Low_Temper), resources.getString(R.string.Low_Temper_Message))
                }
            }

            CoroutineScope(Main).launch {
                if(!isInBackground){
                    val manager = supportFragmentManager
                    CustomDialog(resources.getString(R.string.Temperature_is_too_low), resources.getString(R.string.Please_check_the_diaper)).show(manager, "customDialog")
                }
            }

            isLowTemp = false
        }
    }

    override fun onBackPressed() {
        if(binding.mainDrawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.mainDrawerLayout.closeDrawers()
        }else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        if(!isLanguageChange){
//            stopScan()
            CoroutineScope(IO).launch {
                super.onDestroy()
                disconnectGattServer()
                stopScan()
            }
        }else {
            super.onDestroy()
        }
    }
}
