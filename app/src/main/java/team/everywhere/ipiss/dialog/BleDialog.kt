package team.everywhere.ipiss.dialog

import android.Manifest
import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.MainViewModel
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.adapter.BleDeviceAdapter
import team.everywhere.ipiss.databinding.FragmentBleDialogBinding

class BleDialog : DialogFragment(), BleDeviceAdapter.OnClickListener {
    private var _binding: FragmentBleDialogBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel: MainViewModel by activityViewModels()

    var deviceList = arrayListOf<BluetoothDevice>()

    companion object {
        const val TAG = "BleDialog"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentBleDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        bindViews()
        observeViewModel()
    }

    private fun init() = with(binding){
        val bleDeviceAdapter = BleDeviceAdapter(this@BleDialog, requireContext(), deviceList)
        rvDevice.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = bleDeviceAdapter
        }
    }

    private fun bindViews() = with(binding){
        binding.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    private fun observeViewModel() = with(binding){
        deviceList.clear()
        mainViewModel.scanDevices.observe(viewLifecycleOwner){
//            binding.rvDevice.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
//            binding.rvDevice.adapter = BleDeviceAdapter(this@BleDialog, requireContext(), it)
            val bleDeviceAdapter = BleDeviceAdapter(this@BleDialog, requireContext(), it)
            rvDevice.adapter = bleDeviceAdapter
            rvDevice.scrollToPosition(it.size -1)
        }
    }

    override fun onClickItem(device: BluetoothDevice) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                return
            }
        }

        CoroutineScope(Dispatchers.Main).launch {
            dismiss()
            mainViewModel.sendIsConnecting(true)
            mainViewModel.sendNewDeviceName(device)
            MyApplication.prefs.setString("device", device.name)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        (requireActivity() as MainActivity).stopScan()
        (requireActivity() as MainActivity).homeFragmentGo = false
        _binding = null
    }
}