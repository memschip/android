package team.everywhere.ipiss.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.FragmentSensitivityDialogBinding

class SensitivityDialog() : DialogFragment() {
    private var _binding: FragmentSensitivityDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        var isCheckedSensitive = MyApplication.prefs.getSensitiveIsChecked("isChecked", "Obtuseness")
        Log.d("TAG", "onViewCreated: isCheckedSensitive $isCheckedSensitive")
        when(isCheckedSensitive){
            "Obtuseness" -> {
                binding.cbObtuseness.isChecked = true
                binding.cbBalance.isChecked = false
                binding.cbSensitive.isChecked = false
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Obtuseness")
                MyApplication.prefs.setSensitive("sensitive", 80.0f)
            }
            "Balance" -> {
                binding.cbObtuseness.isChecked = false
                binding.cbBalance.isChecked = true
                binding.cbSensitive.isChecked = false
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Balance")
                MyApplication.prefs.setSensitive("sensitive", 70.0f)
            }
            else -> {
                binding.cbObtuseness.isChecked = false
                binding.cbBalance.isChecked = false
                binding.cbSensitive.isChecked = true
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Sensitive")
                MyApplication.prefs.setSensitive("sensitive", 60.0f)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSensitivityDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cbObtuseness.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setSensitive("sensitive", 80.0f)
                binding.cbBalance.isChecked = false
                binding.cbSensitive.isChecked = false
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Obtuseness")
            }
        }

        binding.cbBalance.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setSensitive("sensitive", 70.0f)
                binding.cbObtuseness.isChecked = false
                binding.cbSensitive.isChecked = false
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Balance")
            }
        }

        binding.cbSensitive.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                Log.d("TAG", "onViewCreated: 민감하다 ")
                MyApplication.prefs.setSensitive("sensitive", 60.0f)
                binding.cbObtuseness.isChecked = false
                binding.cbBalance.isChecked = false
                MyApplication.prefs.setSensitiveIsChecked("isChecked", "Sensitive")
            }
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}