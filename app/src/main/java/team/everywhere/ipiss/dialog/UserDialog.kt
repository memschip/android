package team.everywhere.ipiss.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.FragmentUserDialogBinding


class UserDialog : DialogFragment() {
    private var _binding: FragmentUserDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        var isCheckedPerson = MyApplication.prefs.getPersonIsChecked("isPerson", "Child")
        when(isCheckedPerson){
            "Child" -> {
                binding.cbChild.isChecked = true
                binding.cbPerson.isChecked = false
            }
            else -> {
                binding.cbChild.isChecked = false
                binding.cbPerson.isChecked = true
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentUserDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cbChild.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                binding.cbPerson.isChecked = false
                MyApplication.prefs.setPersonIsChecked("isPerson", "Child")
            }
        }

        binding.cbPerson.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                binding.cbChild.isChecked = false
                MyApplication.prefs.setPersonIsChecked("isPerson", "Person")
            }
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}