package team.everywhere.ipiss.dialog

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.MainViewModel
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.FragmentDoNotDisturbBinding
import team.everywhere.ipiss.util.DoNotDisturbDay
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*

//TODO: 방해금지모드(정해진 날 정해진 시간동안 푸시알림을 받지 않는다)
//예를들어 시작시간 15시00분 종료시간 16시00분
//현재시간이랑 분 값을 가져온다
//시간을 분으로 환산한다 15*60(1시간은 60분이니까 시간을 분으로 계산)+분 < 현재시간*60+현재분 < 16*60+분
//시작시간 <= 현재시간 <= 종료시간 해당값이 true 인 경우 알림을 금지한다.

//TODO 로직
//방해금지모드 활성화는 실시간으로 적용된다. 단, 시간설정 및 요일설정은 실시간 반영되지 않고 기기를 다시 연결했을때 적용된다.

class DoNotDisturbFragment : DialogFragment() {
    private var _binding: FragmentDoNotDisturbBinding? = null
    private val binding get() = _binding!!
    private val mainViewModel: MainViewModel by activityViewModels()

    companion object{
        const val TAG = "DoNotDisturbFragment"
    }

    var isDoNotDisturb: Boolean = MyApplication.prefs.getDoNotDisturb("disturb", false)
    val doNotDisturbDay = DoNotDisturbDay()

    var startHour = 0
    var startMinute = 0
    var endHour = 0
    var endMinute = 0

    var isEveryDay: Boolean = MyApplication.prefs.getEveryDay("everyDay", false)
    var isMonday: Boolean = MyApplication.prefs.getMonday("monday", false)
    var isTuesday: Boolean = MyApplication.prefs.getTuesday("tuesday", false)
    var isWednesday: Boolean = MyApplication.prefs.getWednesday("wednesday", false)
    var isThursday: Boolean = MyApplication.prefs.getThursday("thursday", false)
    var isFriday: Boolean = MyApplication.prefs.getFriday("friday", false)
    var isSaturday: Boolean = MyApplication.prefs.getSaturday("saturday", false)
    var isSunday: Boolean = MyApplication.prefs.getSunday("sunday", false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentDoNotDisturbBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        bindViews()
    }

    private fun init() = with(binding){
        var startTime = MyApplication.prefs.getStartTimeDoNotDisturb("startTime", 22)
        if(startTime == 0){
            tvStartTime.text = "00"
        }else {
            var numberString = String.format("%02d", startTime)
            tvStartTime.text = numberString
        }

        var startMinute = MyApplication.prefs.getStartMinuteDoNotDisturb("startMinute", 0)
        if(startMinute == 0){
            tvStartMinute.text = "00"
        }else {
            var numberString = String.format("%02d", startMinute)
            tvStartMinute.text = numberString
        }

        var endTime = MyApplication.prefs.getEndTimeDoNotDisturb("endTime", 22)
        if(endTime == 0){
            tvEndTime.text = "00"
        }else {
            var numberString = String.format("%02d", endTime)
            tvEndTime.text = numberString
        }

        var endMinute = MyApplication.prefs.getEndMinuteDoNotDisturb("endMinute", 0)
        if(endMinute == 0){
            tvEndMinute.text = "00"
        }else {
            var numberString = String.format("%02d", endMinute)
            tvEndMinute.text = numberString
        }

        //요일체크
        doNotDisturbDay.everyDayCheck(binding, isEveryDay)
        doNotDisturbDay.isMondayCheck(binding, isMonday)
        doNotDisturbDay.isTuesdayCheck(binding, isTuesday)
        doNotDisturbDay.isWednesdayCheck(binding, isWednesday)
        doNotDisturbDay.isThursdayCheck(binding, isThursday)
        doNotDisturbDay.isFridayCheck(binding, isFriday)
        doNotDisturbDay.isSaturdayCheck(binding, isSaturday)
        doNotDisturbDay.isSundayCheck(binding, isSunday)
    }

    private fun bindViews() = with(binding){
        //활성화체크
        swDoNotDisturb.isChecked = isDoNotDisturb
        swDoNotDisturb.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setDoNotDisturb("disturb", true)
                mainViewModel.sendIsDoNotDisturb(true)
            }else {
                MyApplication.prefs.setDoNotDisturb("disturb", false)
                mainViewModel.sendIsDoNotDisturb(false)
            }
        }

        cl.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)

                startHour = SimpleDateFormat("HH").format(cal.time).toInt()
                startMinute = SimpleDateFormat("mm").format(cal.time).toInt()

                if(hour == 0){
                    tvStartTime.text = "00"
                }else {
                    var numberString = String.format("%02d", hour)
                    tvStartTime.text = numberString
                }

                if(minute == 0){
                    binding.tvStartMinute.text = "00"
                }else {
                    var numberString = String.format("%02d", minute)
                    tvStartMinute.text = numberString
                }

                MyApplication.prefs.setStartTimeDoNotDisturb("startTime", hour)
                MyApplication.prefs.setStartMinuteDoNotDisturb("startMinute", minute)
                mainViewModel.sendIsDoNotDisturbStart(true)

//                checkDisturb(startHour, startMinute, endHour, endMinute)
            }

            val timePicker = TimePickerDialog(context, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true)
            timePicker.show()
            timePicker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK)
        }

        cl2.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)

                endHour = SimpleDateFormat("HH").format(cal.time).toInt()
                endMinute = SimpleDateFormat("mm").format(cal.time).toInt()

                if(hour == 0){
                    tvEndTime.text = "00"
                }else {
                    var numberString = String.format("%02d", hour)
                    tvEndTime.text = numberString
                }

                if(minute == 0){
                    tvEndMinute.text = "00"
                }else {
                    var numberString = String.format("%02d", minute)
                    tvEndMinute.text = numberString
                }

                MyApplication.prefs.setEndTimeDoNotDisturb("endTime", hour)
                MyApplication.prefs.setEndMinuteDoNotDisturb("endMinute", minute)
                mainViewModel.sendIsDoNotDisturbEnd(true)

//                checkDisturb(startHour, startMinute, endHour, endMinute)
            }

            val timePicker = TimePickerDialog(context, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true)
            timePicker.show()
            timePicker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK)
        }

        btnCancel.setOnClickListener { dismiss() }
    }

    private fun checkDisturb(startTime: Int, startMinute: Int, endTime: Int, endMinute: Int) = with(binding){

        //00시는 24시를 말한다. 00시 입력시 startTime값 0으로 나온다
        var disturbStart = if(startTime == 0){
            24*60+startMinute
        }else {
            startTime*60+startMinute
        }

//        Timer().scheduleAtFixedRate(object : TimerTask() {
//            override fun run() {
//                val localTime = LocalDateTime.now()
//                val currentTime = localTime.hour*60+localTime.minute
//                Log.d(MainActivity.TAG, "run: currentTime $currentTime")
//                CoroutineScope(Dispatchers.IO).launch {
//                    mainViewModel.sendCurrentTime(currentTime)
//                }
//            }
//        },0, 1000)

        var disturbEnd = if(endTime == 0){
            24*60+endMinute
        }else {
            endTime*60+endMinute
        }

        CoroutineScope(Dispatchers.Main).launch {
            mainViewModel.realCurrentTime.observe(this@DoNotDisturbFragment){
                val isDisturb = it in disturbStart..disturbEnd
                Log.d(TAG, "checkDisturb: isDisturb $isDisturb")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainViewModel.disturbStartText(binding.tvStartTime.text.toString(), binding.tvStartMinute.text.toString())
        mainViewModel.disturbEndText(binding.tvEndTime.text.toString(), binding.tvEndMinute.text.toString())
        _binding = null
    }
}