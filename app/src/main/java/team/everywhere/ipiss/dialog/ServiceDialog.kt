package team.everywhere.ipiss.dialog

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import team.everywhere.ipiss.WebViewActivity
import team.everywhere.ipiss.databinding.FragmentServiceDialogBinding


class ServiceDialog : DialogFragment() {
    private var _binding: FragmentServiceDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentServiceDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnHomepage.setOnClickListener {
            val intent = Intent(activity, WebViewActivity::class.java)
            startActivity(intent)
            dismiss()
        }

        binding.btnEmail.setOnClickListener {
            val recipient = "sy8259km@gmail.com"
            val intent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto: sy8259km@gmail.com")
                putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
            }
            startActivity(intent)
            dismiss()
        }

        binding.btnCall.setOnClickListener {
            val phoneNumber = "tel:070-4146-6259"
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber))
            startActivity(intent)
            dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}