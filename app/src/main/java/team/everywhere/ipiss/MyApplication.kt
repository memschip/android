package team.everywhere.ipiss

import android.app.Activity
import android.app.Application
import android.os.Bundle
import team.everywhere.ipiss.util.PreferenceUtil

class MyApplication: Application() {

    private var activityCount = 0
    private var isInBackground = true

    companion object {
        lateinit var prefs: PreferenceUtil
    }

    override fun onCreate() {
        super.onCreate()

        prefs = PreferenceUtil(applicationContext)

        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                // Activity created
            }

            override fun onActivityStarted(activity: Activity) {
                // Activity started
                activityCount++
                isInBackground = false
            }

            override fun onActivityResumed(activity: Activity) {
                // Activity resumed
            }

            override fun onActivityPaused(activity: Activity) {
                // Activity paused
            }

            override fun onActivityStopped(activity: Activity) {
                // Activity stopped
                activityCount--
                if (activityCount == 0) {
                    isInBackground = true
                    // App is in the background
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
                // Activity save instance state
            }

            override fun onActivityDestroyed(activity: Activity) {
                // Activity destroyed
            }
        })
    }

    fun isAppInBackground(): Boolean {
        return isInBackground
    }
}