package team.everywhere.ipiss.fragment

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kizitonwose.calendar.core.*
import com.kizitonwose.calendar.view.MonthDayBinder
import com.kizitonwose.calendar.view.MonthHeaderFooterBinder
import com.kizitonwose.calendar.view.ViewContainer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.MainViewModel
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.adapter.HistoryAdapter
import team.everywhere.ipiss.dao.History
import team.everywhere.ipiss.databinding.FragmentHomeBinding
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import team.everywhere.ipiss.R
import team.everywhere.ipiss.databinding.Example3CalendarDayBinding
import team.everywhere.ipiss.databinding.Example3CalendarHeaderBinding
import team.everywhere.ipiss.util.*
import team.everywhere.ipiss.util.getColorCompat
import team.everywhere.ipiss.util.setTextColorRes

data class Event(val id: String, val text: String, val date: LocalDate)

class HomeFragment : BaseFragment(R.layout.fragment_home), HasBackButton {
    private var _binding: FragmentHomeBinding? = null
    lateinit var binding: FragmentHomeBinding

    lateinit var mainActivity: MainActivity

    lateinit var historyAdapter: HistoryAdapter
    private val histories: ArrayList<History> = ArrayList()
    lateinit var month: YearMonth
    var temperature: Float = 0.0f
    var humidity: Float = 0.0f

    companion object{
        const val TAG = "HomeFragment"
    }

    private val mainViewModel: MainViewModel by activityViewModels()

    override val titleRes: Int = R.string.example_3_title
    var language = MyApplication.prefs.getLanguage("language", false)

    private var selectedDate: LocalDate? = null
    @RequiresApi(Build.VERSION_CODES.O)
    private val today = LocalDate.now()

    @RequiresApi(Build.VERSION_CODES.O)
    private val titleSameYearFormatter = DateTimeFormatter.ofPattern("MMMM")
    @RequiresApi(Build.VERSION_CODES.O)
    private val titleFormatter = if(language){
        DateTimeFormatter.ofPattern("yyyy.MM")
    }else {
        DateTimeFormatter.ofPattern("yyyy년 MM월")
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private val selectionFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd") //d MMM yyyy
    private val events = mutableMapOf<LocalDate, List<Event>>()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = requireActivity() as MainActivity
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = _binding!!

        //달력 오른쪽으로 이동
        (requireActivity() as MainActivity).binding.btnRight.setOnClickListener {
            binding.exThreeCalendar.findFirstVisibleMonth()?.let {
                binding.exThreeCalendar.smoothScrollToMonth(it.yearMonth.nextMonth)

                month = it.yearMonth.nextMonth
                val a = month.toString().split("-")[1]
                binding.tvMonth.text = a
            }

            updateMonthCount()
        }
        //달력 왼쪽으로 이동
        (requireActivity() as MainActivity).binding.btnLeft.setOnClickListener {
            binding.exThreeCalendar.findFirstVisibleMonth()?.let {
                binding.exThreeCalendar.smoothScrollToMonth(it.yearMonth.previousMonth)

                month = it.yearMonth.previousMonth
                val a = month.toString().split("-")[1]
                binding.tvMonth.text = a
            }

            updateMonthCount()
        }
        //달력스크롤
        binding.exThreeCalendar.monthScrollListener = {
            month = it.yearMonth
            val a = month.toString().split("-")[1]
            binding.tvMonth.text = a

            activityToolbarTitle.text = titleFormatter.format(it.yearMonth)
            binding.exThreeCalendar.post {
                selectDate(today)
            }

            updateMonthCount()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addStatusBarColorUpdate(team.everywhere.ipiss.R.color.example_3_statusbar_color)

        Log.d(TAG, "onViewCreated: histories.count ${histories.count()}")
        historyAdapter = HistoryAdapter(requireContext(), histories)
        binding.exThreeRv.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = historyAdapter
        }

        val daysOfWeek = daysOfWeek()
        val currentMonth = YearMonth.now()
        val startMonth = currentMonth.minusMonths(50)
        val endMonth = currentMonth.plusMonths(50)
        configureBinders(daysOfWeek)
        binding.exThreeCalendar.apply {
            setup(startMonth, endMonth, daysOfWeek.first())
            scrollToMonth(currentMonth)
        }

//        TODO:selectDate(today)가 호출되면서 가끔 histories.count가 2가 된다. 그래서 주석처리
//        if (savedInstanceState == null) {
//            Log.d(TAG, "onViewCreated: 이것도호출")
//            binding.exThreeCalendar.post { selectDate(today) }
//        }

//        TODO: 플로팅버튼 클릭하면 dialog 뜬다
//        binding.exThreeAddButton.setOnClickListener {
//            saveEvent("안녕하세요")
//        }

//        mainViewModel.goHum.observe(viewLifecycleOwner){
//            Log.d(TAG, "onViewCreated: humidity $humidity")
//            humidity = it
//        }
//        mainViewModel.goTemp.observe(viewLifecycleOwner){
//            Log.d(TAG, "onViewCreated: temperature $temperature")
//            temperature = it
//        }

        mainViewModel.temp.observe(viewLifecycleOwner){
            Log.d(TAG, "onViewCreated: temperature $temperature")
            temperature = it
        }

        mainViewModel.hum.observe(viewLifecycleOwner) {
            Log.d(TAG, "onViewCreated: humidity $humidity")
            humidity = it
        }
        
        mainViewModel.updateCalendar.observe(viewLifecycleOwner){
            Log.d(TAG, "onViewCreated: viewModel it: $it")
            if(it == "등록"){
                saveEvent("안녕하세요")
                historyAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun selectDate(date: LocalDate) {
        Log.d(TAG, "selectDate 호출: $selectedDate, date $date")  //date가 클릭한 날짜
        CoroutineScope(IO).launch {
            if (selectedDate != date) {
                val oldDate = selectedDate
                selectedDate = date
                withContext(Main){
                    oldDate?.let { binding.exThreeCalendar.notifyDateChanged(it) }
                    binding.exThreeCalendar.notifyDateChanged(date)
                    updateAdapterForDate(date)
                    binding.liMonthCount.visibility = View.GONE
                    binding.liTodayCount.visibility = View.GONE
                }
            }
        }

        histories.clear()

        CoroutineScope(IO).launch {
            mainActivity.db.historyDao().getOneHistory(date.toString()).reversed().forEach { history ->
                histories.add(history)
            }

            var today = LocalDate.now()
            var day = today.toString().split("-")[2]
            var selectedDay = date.toString().split("-")[2]
            withContext(Main){
                if(histories.count() == 0){
                    binding.liMonthCount.visibility = View.VISIBLE
                    binding.liTodayCount.visibility = View.GONE
                }else {
                    binding.liMonthCount.visibility = View.VISIBLE
                    binding.liTodayCount.visibility = View.VISIBLE

                    binding.tvChangeCount.text = if(histories.count() > 10){
                        " ${histories.count()}"
                    }else {
                        " 0${histories.count()}"
                    }

                    if(language){
                        binding.tvDay.visibility = View.GONE
                    }else {
                        if(day == selectedDay){
                            binding.tvDay.text = "금"
                        }else {
                            binding.tvDay.text = selectedDay
                        }
                    }
                }
                historyAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun saveEvent(text: String) {
        if (text.isBlank()) {
            Toast.makeText(requireContext(), R.string.example_3_empty_input_text, Toast.LENGTH_LONG)
                .show()
        } else {
            selectedDate?.let {
                events[it] =
                    events[it].orEmpty().plus(Event(UUID.randomUUID().toString(), text, it))
                //it은 2022-12-06
                CoroutineScope(IO).launch {
                    mainActivity.db.historyDao().insertHistory(History(UUID.randomUUID(), it.toString(), temperature, humidity))
                    withContext(Main){
                        updateAdapterForDate(it)
                    }
                }

//                updateAdapterForDate(it)
            }
        }
    }

    private fun updateAdapterForDate(date: LocalDate) {
//        binding.exThreeSelectedDateText.text = selectionFormatter.format(date) + " 알람기록"
        binding.exThreeSelectedDateText.text = selectionFormatter.format(date)
    }

    private fun updateMonthCount(){
        CoroutineScope(IO).launch {
            var monthCount = 0
            mainActivity.db.historyDao().getAll().forEach { history ->
                if(history.date.split("-")[1] == month.toString().split("-")[1]){
                    monthCount++
                }
            }

            withContext(Main){
                binding.tvMonthChangeCount.text = if(monthCount >= 10){
                    " $monthCount"
                }else {
                    " 0${monthCount}"
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        activityToolbar.setBackgroundColor(
            requireContext().getColorCompat(R.color.example_3_toolbar_color),
        )
    }

    override fun onStop() {
        super.onStop()
        activityToolbar.setBackgroundColor(
            requireContext().getColorCompat(R.color.colorPrimary),
        )
    }

    private fun configureBinders(daysOfWeek: List<DayOfWeek>) {
        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay
            val binding = Example3CalendarDayBinding.bind(view)

            init {
                view.setOnClickListener {
                    if (day.position == DayPosition.MonthDate) {
                        selectDate(day.date)
                    }
                }
            }
        }
        binding.exThreeCalendar.dayBinder = object : MonthDayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, data: CalendarDay) {
                container.day = data
                val textView = container.binding.exThreeDayText
                val dotView = container.binding.exThreeDotView

                textView.text = data.date.dayOfMonth.toString()

                if (data.position == DayPosition.MonthDate) {
                    textView.makeVisible()
                    when (data.date) {
                        today -> {
                            textView.setTextColorRes(R.color.example_3_white)
                            textView.setBackgroundResource(R.drawable.example_3_today_bg)
                            dotView.makeInVisible()
                        }
                        selectedDate -> {
                            textView.setTextColorRes(R.color.example_3_blue)
                            textView.setBackgroundResource(R.drawable.example_3_selected_bg)
                            dotView.makeInVisible()
                        }
                        else -> {
//                            textView.setTextColorRes(R.color.example_3_black)
//                            textView.background = null
//                            dotView.isVisible = events[data.date].orEmpty().isNotEmpty()
                            if (data.date.dayOfWeek.toString() == "SATURDAY") {
                                textView.setTextColor(Color.BLUE)
                            } else if (data.date.dayOfWeek.toString() == "SUNDAY") {
                                textView.setTextColor(Color.RED)
                            } else {
                                textView.setTextColor(Color.BLACK)
                            }
                            textView.background = null
                            dotView.isVisible = events[data.date].orEmpty().isNotEmpty()
                        }
                    }
                } else {
                    textView.makeInVisible()
                    dotView.makeInVisible()
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = Example3CalendarHeaderBinding.bind(view).legendLayout.root
        }
        binding.exThreeCalendar.monthHeaderBinder =
            object : MonthHeaderFooterBinder<MonthViewContainer> {
                override fun create(view: View) = MonthViewContainer(view)
                override fun bind(container: MonthViewContainer, data: CalendarMonth) {
                    // Setup each header day text if we have not done that already.
                    if (container.legendLayout.tag == null) {
                        container.legendLayout.tag = data.yearMonth
                        container.legendLayout.children.map { it as TextView }
                            .forEachIndexed { index, tv ->
                                tv.text = daysOfWeek[index].name.first().toString()
                                tv.setTextColorRes(R.color.example_3_black)
                            }
                    }
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}