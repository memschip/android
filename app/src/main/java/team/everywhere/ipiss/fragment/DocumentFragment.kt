package team.everywhere.ipiss.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R
import team.everywhere.ipiss.databinding.FragmentDocumentBinding
import java.io.InputStream


class DocumentFragment : Fragment() {
    private var _binding: FragmentDocumentBinding? = null
    private val binding get() = _binding!!
    
    companion object {
        const val TAG = "DocumentFragment"
    }

    var language = MyApplication.prefs.getLanguage("language", false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentDocumentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        init()
    }
    
    private fun init() = with(binding){
        Log.d(TAG, "init: language $language")
        if(language){
//            iv.setImageResource(R.drawable.bg_document_en2)
            val pdfRawResourceId = R.raw.english
            val pdfStream: InputStream = resources.openRawResource(pdfRawResourceId)

            pdfView.fromStream(pdfStream)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .pageFling(true)
                .onPageChange { page, pageCount -> /* Handle page change here */ }
                .pageSnap(true)
                .autoSpacing(true)
                .load()
        }else {
//            iv.setImageResource(R.drawable.bg_document)
            val pdfRawResourceId = R.raw.korea
            val pdfStream: InputStream = resources.openRawResource(pdfRawResourceId)

            pdfView.fromStream(pdfStream)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .pageFling(true)
                .onPageChange { page, pageCount -> /* Handle page change here */ }
                .pageSnap(true)
                .autoSpacing(true)
                .load()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}