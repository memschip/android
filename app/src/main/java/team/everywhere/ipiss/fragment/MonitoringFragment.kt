package team.everywhere.ipiss.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import team.everywhere.ipiss.MainViewModel
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R
import team.everywhere.ipiss.databinding.FragmentMonitoringBinding
import team.everywhere.ipiss.util.UpdateTime2
import java.util.*
import kotlin.concurrent.timerTask


class MonitoringFragment : Fragment() {
//    private var _binding: FragmentMonitoringBinding? = null
//    private val binding get() = _binding!!

    lateinit var binding: FragmentMonitoringBinding

    companion object{
        const val TAG = "MonitoringFragment"
    }

    private val mainViewModel: MainViewModel by activityViewModels()

    lateinit var updateTime: UpdateTime2
    var isTempSensorStart = MyApplication.prefs.getIsTempSensorStart("isTempStart", true)

    var language = MyApplication.prefs.getLanguage("language", false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        _binding = FragmentMonitoringBinding.inflate(inflater, container, false)
        binding = FragmentMonitoringBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        observeViewModel()
    }

    private fun init() = with(binding){
        Timer().scheduleAtFixedRate(timerTask {
            updateTime = UpdateTime2()
            CoroutineScope(Dispatchers.Main).launch {
                binding.tvDate.text = updateTime.getToday(language)
                binding.tvTime.text = updateTime.getCurrentTime()
            }
        },0, 500)
    }

    private fun observeViewModel() = with(binding){
        mainViewModel.temp.observe(viewLifecycleOwner){
            if(language){
//                binding.tvT.text = "${celsiusToFahrenheit(it.toInt())}${resources.getString(R.string.celsius)}"
                binding.tvT.text = "${celsiusToFahrenheit(it)}${resources.getString(R.string.celsius)}"
            }else {
                binding.tvT.text = "${it.toString()}${resources.getString(R.string.celsius)}"
            }
//            binding.tvT.text = "${it.toString()}${resources.getString(R.string.celsius)}"
            binding.cpbT.progress = it
        }

        mainViewModel.hum.observe(viewLifecycleOwner) {
            binding.tvH.text = it.toString()+"%RH"
            binding.cpbH.progress = it
        }
    }

    private fun celsiusToFahrenheit(celsius: Float): Double {
        return roundTo1DecimalPlace((celsius * 9/5) + 32)
    }

    private fun roundTo1DecimalPlace(number: Float): Double {
        val roundedNumber = Math.round(number*10.0) / 10.0
        return roundedNumber
    }

    override fun onDestroy() {
        super.onDestroy()
//        _binding = null
    }
}