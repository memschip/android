package team.everywhere.ipiss.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import team.everywhere.ipiss.MainActivity
import team.everywhere.ipiss.MainViewModel
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.FragmentSettingBinding
import team.everywhere.ipiss.dialog.*


class SettingFragment : Fragment() {
    private var _binding: FragmentSettingBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel: MainViewModel by activityViewModels()

    companion object {
        const val TAG = "SettingFragment"
    }

    lateinit var mainActivity: MainActivity
    var isAutoStart: Boolean = false
    var isTempSensorStart: Boolean = true
    var isVibrationStart: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = requireActivity() as MainActivity

        isAutoStart = MyApplication.prefs.getIsAutoStart("autoStart", false)
        isTempSensorStart = MyApplication.prefs.getIsTempSensorStart("isTempStart", true)
        isVibrationStart = MyApplication.prefs.getIsVibrationStart("isVibration", false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSettingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindViews()
        disturbTimeSetting()
    }

    private fun bindViews() = with(binding){
        swAutoStart.isChecked = isAutoStart
        swAutoStart.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setIsAutoStart("autoStart", true)
            }else {
                MyApplication.prefs.setIsAutoStart("autoStart", false)
            }
        }

        cl1.setOnClickListener {
            (requireActivity() as MainActivity).isDeviceSearchStart = true
            (requireActivity() as MainActivity).scanResults?.clear()
            val manager = requireActivity().supportFragmentManager
            BleDialog().show(manager, "bleDialog")
            mainActivity.startScan()
        }

        cl2.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            SensitivityDialog().show(manager, "sensitiveDialog")
        }

        cl3.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            UserDialog().show(manager, "userDialog")
        }

        swTempSensorStart.isChecked = isTempSensorStart
        swTempSensorStart.setOnCheckedChangeListener { compoundButton, b ->
            Log.d(TAG, "bindViews: b $b")
            if(b){
                MyApplication.prefs.setIsTempSensorStart("isTempStart", true)
                mainViewModel.sendIsTempSensor(true)
            }else {
                MyApplication.prefs.setIsTempSensorStart("isTempStart", false)
                mainViewModel.sendIsTempSensor(false)
            }
        }

        swVibrationStart.isChecked = isVibrationStart
        swVibrationStart.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                MyApplication.prefs.setIsVibrationStart("isVibration", true)
            }else {
                MyApplication.prefs.setIsVibrationStart("isVibration", false)
            }

            (requireActivity() as MainActivity).navSwitch()
        }
        
        cl6.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            DoNotDisturbFragment().show(manager, "doNotDisturbFragment")
        }

        cl5.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            SoundsFragment().show(manager, "soundsFragment")
        }

        cl7.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            InformationFragment().show(manager, "informationFragment")
        }
    }

    private fun disturbTimeSetting(){
        var startTime = MyApplication.prefs.getStartTimeDoNotDisturb("startTime", 22)
        var realStartTime = if(startTime == 0){
            "00"
        }else {
            var numberString = String.format("%02d", startTime)
            numberString
        }

        var startMinute = MyApplication.prefs.getStartMinuteDoNotDisturb("startMinute", 0)
        var realStartMinute = if(startMinute == 0){
            "00"
        }else {
            var numberString = String.format("%02d", startMinute)
            numberString
        }

        var endTime = MyApplication.prefs.getEndTimeDoNotDisturb("endTime", 22)
        var realEndTime = if(endTime == 0){
            "00"
        }else {
            var numberString = String.format("%02d", endTime)
            numberString
        }

        var endMinute = MyApplication.prefs.getEndMinuteDoNotDisturb("endMinute", 0)
        var realEndMinute = if(endMinute == 0){
            "00"
        }else {
            var numberString = String.format("%02d", endMinute)
            numberString
        }

        mainViewModel.disturbStartHour.observe(viewLifecycleOwner){
            realStartTime = it
            binding.tvDoNotDistrubTime.text = "$realStartTime:$realStartMinute ~ $realEndTime:$realEndMinute"
        }

        mainViewModel.disturbStartMinute.observe(viewLifecycleOwner){
            realStartMinute = it
            binding.tvDoNotDistrubTime.text = "$realStartTime:$realStartMinute ~ $realEndTime:$realEndMinute"
        }

        mainViewModel.disturbEndHour.observe(viewLifecycleOwner){
            realEndTime = it
            binding.tvDoNotDistrubTime.text = "$realStartTime:$realStartMinute ~ $realEndTime:$realEndMinute"
        }

        mainViewModel.disturbEndMinute.observe(viewLifecycleOwner){
            realEndMinute = it
            binding.tvDoNotDistrubTime.text = "$realStartTime:$realStartMinute ~ $realEndTime:$realEndMinute"
        }

        binding.tvDoNotDistrubTime.text = "$realStartTime:$realStartMinute ~ $realEndTime:$realEndMinute"
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}