package team.everywhere.ipiss.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.ItemSoundBinding

class SoundAdapter(var context: Context, var onClickListener: OnClickListener): RecyclerView.Adapter<SoundAdapter.ViewHolder>() {

    interface OnClickListener {
        fun onClickSound(sound: String)
    }

    var soundName = MyApplication.prefs.getSoundName("soundName", "")
    var sounds = arrayListOf<String>(
        "아기울음소리", "아기울음소리2", "아기울음소리3", "아기소리", "Sound", "Sound01", "Sound03",
        "Relaxing_Light", "Relaxed_Sound", "Powerful_Electric", "Powerful_Beat", "Light_Sound")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemSoundBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(sounds[position])
    }

    override fun getItemCount(): Int {
        return sounds.size
    }

    inner class ViewHolder(private var binding: ItemSoundBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(sound: String){
            when(soundName){

            }
            binding.tvSoundName.text = sound
            binding.cbSound.setOnCheckedChangeListener { compoundButton, b ->
                if(b){
                    onClickListener.onClickSound(sound)
                }
            }
        }
    }
}