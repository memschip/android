package team.everywhere.ipiss.adapter

import android.Manifest
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.databinding.ItemDeviceBinding

class BleDeviceAdapter(
    var onClickListener: OnClickListener,
    var context: Context,
    var devices: ArrayList<BluetoothDevice>): RecyclerView.Adapter<BleDeviceAdapter.ViewHolder>() {

    companion object {
        const val TAG = "BleDeviceAdapter"
    }

    interface OnClickListener {
        fun onClickItem(device: BluetoothDevice)
    }

    var baseDevice = MyApplication.prefs.getString("device", "없다")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemDeviceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(devices.size != 0){
            holder.bind(devices[position])
        }
    }

    override fun getItemCount(): Int {
        return if(devices.size != 0){
            devices.size
        }else {
            0
        }
    }

    inner class ViewHolder(private var binding: ItemDeviceBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(device: BluetoothDevice){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
            }

            if(devices.size != 0){
                binding.tvDeviceName.text = device.name
//                binding.cbDevice.isChecked = binding.tvDeviceName.text == baseDevice
                binding.cbDevice.setOnCheckedChangeListener { compoundButton, bool ->
                    Log.d(TAG, "연결 디바이스 체크여부 확인 $bool")
                    if(bool){
                        Log.d("TAG", "adapterPosition $adapterPosition position $oldPosition absoluteAdapterPosition $absoluteAdapterPosition bindingAdapterPosition $bindingAdapterPosition")
                        try {
                            onClickListener.onClickItem(devices[absoluteAdapterPosition])
                        }catch (e: java.lang.IndexOutOfBoundsException){
                            Log.d(TAG, "BleDeviceError $e")
                        }
                    }else {
//                        MyApplication.prefs.setString("device", "없다")
                    }
                }
            }
        }
    }
}