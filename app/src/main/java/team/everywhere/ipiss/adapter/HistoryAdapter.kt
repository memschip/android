package team.everywhere.ipiss.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R
import team.everywhere.ipiss.dao.History
import team.everywhere.ipiss.databinding.Example3EventItemViewBinding
import team.everywhere.ipiss.util.UpdateTime
import java.util.*
import kotlin.collections.ArrayList


class HistoryAdapter(var context: Context, var histories: ArrayList<History>?): RecyclerView.Adapter<HistoryAdapter.ViewHolder>(){
    lateinit var updateTime: UpdateTime
    var language = MyApplication.prefs.getLanguage("language", false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(Example3EventItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(histories!!.count() != 0){
            holder.bind(histories!![position])
        }
    }

    override fun getItemCount(): Int {
        return histories!!.count()
    }

    inner class ViewHolder(private var binding: Example3EventItemViewBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(history: History){
            updateTime = UpdateTime(history)
            var time = history.updateTime.split(" ")[1]
            var hour = time.split(":")[0]
            var minute = time.split(":")[1]

            var temp = history.temperature
            var hum = history.humidity

            if(language){
//                binding.itemEventTime.text = updateTime.getTimeDiffEng()
                binding.itemEventText.text = "${hour}:${minute} ${temp}${context.resources.getString(
                    R.string.celsius)}, ${hum}%RH diaper changed"
            }else {
//                binding.itemEventTime.text = updateTime.getTimeDiffKor()
                binding.itemEventText.text = "${hour}시 ${minute}분 ${temp}${context.resources.getString(
                    R.string.celsius)}, ${hum}%RH 기저귀를 교환했습니다."
            }
        }
    }
}