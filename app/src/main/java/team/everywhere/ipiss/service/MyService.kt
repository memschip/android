package team.everywhere.ipiss.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.IBinder
import androidx.core.app.NotificationCompat
import team.everywhere.ipiss.MyApplication
import team.everywhere.ipiss.R

class MyService : Service() {
    companion object {
        const val NOTIFICATION_ID = 10
        val CHANNEL_ID = "vibration"
        val CHANNEL_ID2 = "novibration"
    }

    var isVibration = MyApplication.prefs.getIsVibrationStart("isVibration", false)
    var language = MyApplication.prefs.getLanguage("language", false)

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if(isVibration){
//                createNotificationChannel()
//                val notification = NotificationCompat.Builder(this, CHANNEL_ID)
//                    .setContentTitle("아이피스")
//                    .setContentText("MyService is running")
//                    .setSmallIcon(R.drawable.logo2)
//                    .setPriority(NotificationCompat.PRIORITY_HIGH)
//                    .build()
//                startForeground(NOTIFICATION_ID, notification)
//            }else {
//                createNotificationChannel()
//                val notification = NotificationCompat.Builder(this, CHANNEL_ID2)
//                    .setContentTitle("아이피스")
//                    .setContentText("MyService is running")
//                    .setSmallIcon(R.drawable.logo2)
//                    .setPriority(NotificationCompat.PRIORITY_LOW)
//                    .build()
//                startForeground(NOTIFICATION_ID, notification)
//            }
//        }
        if(language){
            createNotificationChannel()
            val notification = NotificationCompat.Builder(this, CHANNEL_ID2)
                .setContentTitle("IPISS")
                .setContentText("IPISS is running")
                .setSmallIcon(R.drawable.logo2)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .build()
            startForeground(NOTIFICATION_ID, notification)
        }else {
            createNotificationChannel()
            val notification = NotificationCompat.Builder(this, CHANNEL_ID2)
                .setContentTitle("IPISS")
                .setContentText("IPISS가 실행 중입니다.")
                .setSmallIcon(R.drawable.logo2)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .build()
            startForeground(NOTIFICATION_ID, notification)
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun createNotificationChannel() {
//        if(isVibration){
//            val notificationChannel = NotificationChannel(CHANNEL_ID, "MyApp notification", NotificationManager.IMPORTANCE_HIGH
//            )
//            notificationChannel.enableLights(true)
//            notificationChannel.lightColor = Color.RED
//            notificationChannel.description = "AppApp Tests"
//
//            val notificationManager = applicationContext.getSystemService(
//                Context.NOTIFICATION_SERVICE) as NotificationManager
//            notificationManager.createNotificationChannel(
//                notificationChannel)
//        }else {
//            val notificationChannel = NotificationChannel(CHANNEL_ID2, "MyApp notification", NotificationManager.IMPORTANCE_LOW
//            )
//            notificationChannel.enableLights(true)
//            notificationChannel.lightColor = Color.RED
//            notificationChannel.description = "AppApp Tests"
//
//            val notificationManager = applicationContext.getSystemService(
//                Context.NOTIFICATION_SERVICE) as NotificationManager
//            notificationManager.createNotificationChannel(
//                notificationChannel)
//        }
        val notificationChannel = NotificationChannel(
            CHANNEL_ID2, "MyApp notification", NotificationManager.IMPORTANCE_LOW
        )
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.RED
        notificationChannel.description = "AppApp Tests"

        val notificationManager = applicationContext.getSystemService(
            Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(
            notificationChannel)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}