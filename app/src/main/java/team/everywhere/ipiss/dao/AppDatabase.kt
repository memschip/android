package team.everywhere.ipiss.dao

import androidx.room.Database
import androidx.room.RoomDatabase

//Database에 필요한거
//1.Database
//2.table == entity
//3.dao    interface

@Database(entities = [History::class], version = 2)
abstract class AppDatabase: RoomDatabase() {
    abstract fun historyDao(): HistoryDao
}

