package team.everywhere.ipiss.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID

@Entity
data class History(
    @PrimaryKey
    val idx: UUID,
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "temperature")
    val temperature: Float,
    @ColumnInfo(name = "humidity")
    val humidity: Float,
    @ColumnInfo(name = "description")
    val description: String = "기저귀를 교환하셨습니다.",
    @ColumnInfo(name = "update_time")
    val updateTime: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date()).toString()
)
