package team.everywhere.ipiss.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {
    @Query("SELECT * FROM History")
    fun getAll(): List<History>

    @Insert
    fun insertHistory(history: History)

    @Query("SELECT * FROM HISTORY Where date = :date")
    fun getOneHistory(date: String): List<History>

    @Query("DELETE FROM History")
    fun getAllDelete()
}