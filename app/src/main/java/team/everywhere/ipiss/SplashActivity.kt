package team.everywhere.ipiss

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import team.everywhere.ipiss.databinding.ActivitySplashBinding
import java.util.*

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    
    companion object{
        const val TAG = "SplashActivity"
    }

    var language = MyApplication.prefs.getLanguage("language", false)

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        checkPermission()
        requestPermission()
        autoStartPermission()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            val permission = arrayOf(
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.POST_NOTIFICATIONS,
                Manifest.permission.SYSTEM_ALERT_WINDOW
            )
            requestPermissions(permission, 100)

        }else{
            val permission = arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.SYSTEM_ALERT_WINDOW
            )
            requestPermissions(permission, 100)
        }
    }

    private fun checkPermission(){
        Log.d(TAG, "checkPermission: 호출")
        var permission = mutableMapOf<String, String>()
        permission["alert"] = Manifest.permission.SYSTEM_ALERT_WINDOW

        var denied = permission.count {
            ContextCompat.checkSelfPermission(this, it.value) == PackageManager.PERMISSION_GRANTED
        }

        if(denied > 0){
            requestPermissions(permission.values.toTypedArray(), 1)
        }
    }

    //TODO: 자동실행 권한
    private fun autoStartPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                val intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + this.packageName)
                )
                startActivityForResult(intent, 232)
            } else {
                //Permission Granted-System will work
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 100){
            var count = grantResults.count() {
                it == PackageManager.PERMISSION_GRANTED
            }

            if(count >= 2){
                CoroutineScope(Dispatchers.Main).launch {
                    delay(2000)
                    if(language){
                        val configuration = Configuration()
                        configuration.locale = Locale.US
                        resources.updateConfiguration(configuration, resources.displayMetrics)
                    }else {
                        val configuration = Configuration()
                        configuration.locale = Locale.KOREA
                        resources.updateConfiguration(configuration, resources.displayMetrics)
                    }

                    val loginIntent = Intent(baseContext, MainActivity::class.java)
                    startActivity(loginIntent)
                    finish()
                }
            }
        }else if(requestCode == 1){
            grantResults.forEach {
                if(it == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(applicationContext, "서비스의 필요한 권한입니다.\n권한에 동의해주세요.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}