package team.everywhere.ipiss

import android.app.PendingIntent
import android.app.PendingIntent.CanceledException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log


class MyAutoRunApp: BroadcastReceiver() {

    companion object {
        const val TAG = "MyAutoRunApp"
    }

    var isAutoStart = MyApplication.prefs.getIsAutoStart("autoStart", false)

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive: 재실행 호출 ${intent.action}")
//        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.action) && isAutoStart){
//            val i = Intent(context, SplashActivity::class.java)
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            context.startActivity(i)
//        }

        if (intent.action == Intent.ACTION_BOOT_COMPLETED && isAutoStart) {
            val i = Intent(context, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(i)
        }

//        if (intent.action.equals("android.intent.action.BOOT_COMPLETED")) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                val i = Intent(context, MainActivity::class.java)
//                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                val pendingIntent =
//                    PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)
//                try {
//                    pendingIntent.send()
//                } catch (e: CanceledException) {
//                    e.printStackTrace()
//                }
//            } else {
//                val i = Intent(context, MainActivity::class.java)
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                context.startActivity(i)
//            }
//        }
    }
}